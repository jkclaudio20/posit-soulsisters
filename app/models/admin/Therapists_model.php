<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Therapists_model extends CI_Model {
	/**
	 * Holds and array of table names
	 *
	 * @var array $tables
	 */
	public $tables;

	public function __construct()
	{
		parent::__construct();
		$this->tables = $this->config->item('tables', 'pos');
	}

    /**
     * Add Therapist
     *
     * @param  array $therapist_data
     * @return int|boolean
     * @author Karlo
     */
    public function add_therapist(array $therapist_data)
    {
        if ($this->db->insert($this->tables['therapists'], $therapist_data))
        {
            $this->ion_auth->set_message("{$therapist_data['first_name']} {$therapist_data['last_name']} successfully added to therapists.");
            return $this->db->insert_id();
        }
        else
        {
            $this->ion_auth->set_error('An error has occured while trying to add therapist. Please try again.');
            return FALSE;
        }
    }

    /**
     * Get Therapist Data
     *
     * @param  int $therapist_id
     * @return object
     * @author Karlo
     */
    public function get_therapist_data($therapist_id)
    {
        $query = $this->db->where('therapist_id', $therapist_id)
                          ->limit(1)
                          ->get($this->tables['therapists']);

        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    /**
     * Update Therapist
     *
     * @param  int   $therapist_id
     * @param  array $therapist_data
     * @return int|boolean
     * @author Karlo
     */
    public function update_therapist($therapist_id, array $therapist_data)
    {
        if ($this->db->update($this->tables['therapists'], $therapist_data, array('therapist_id' => $therapist_id)))
        {
            $this->ion_auth->set_message("{$therapist_data['first_name']} {$therapist_data['last_name']} successfully updated.");
            return TRUE;
        }
        else
        {
            $this->ion_auth->set_error('An error has occured while trying to update therapist. Please try again.');
            return FALSE;
        }
    }

    /**
     * Delete Therapist
     *
     * @param  int $therapist_id
     * @return boolean
     * @author Karlo
     */
    public function delete_therapist($therapist_id)
    {
        if ($this->db->delete($this->tables['therapists'], array('therapist_id' => $therapist_id)))
        {
            $this->ion_auth->set_message('Therapist successfully deleted.');
            return TRUE;
        }
        else
        {
            $this->ion_auth->set_error('An error has occured while trying to delete therapist. Please try again.');
            return FALSE;
        }
    }

	/**
	 * Get Therapists
	 *
	 * @param  boolean $return_dropdown
	 * @return array|boolean
	 * @author Karlo
	 */
	public function get_therapists($return_dropdown = FALSE)
	{
		$query = $this->db->select("*, CONCAT(first_name, ' ', last_name) AS full_name")
						  ->order_by('first_name')
						  ->get($this->tables['therapists']);

		if ($return_dropdown === TRUE)
		{
			$dropdown = array();
			$dropdown[NULL] = NULL;
			foreach ($query->result() as $row)
			{
				$dropdown[$row->therapist_id] = $row->full_name;
			}
			return $dropdown;
		}
		else
		{
			return ($query->num_rows() > 0) ? $query->result() : FALSE;
		}
	}

	/**
	 * Get Service Data
	 *
	 * @param  int $id
	 * @return object|boolean
	 * @author Karlo
	 */
	public function get_service_data($id)
	{
		$query = $this->db->select("{$this->tables['sale_items']}.id AS primary_id, product_name, {$this->tables['sales']}.date, COALESCE(subtotal, 0) AS subtotal, incentive_amount, COALESCE(subtotal * incentive_amount, 0) AS incentive")
						  ->join($this->tables['sales'], $this->tables['sale_items'].'.sale_id = '.$this->tables['sales'].'.id')
						  ->join($this->tables['products'], $this->tables['sale_items'].'.product_id = '.$this->tables['products'].'.id')
						  ->where($this->tables['sale_items'].'.id', $id)
						  ->limit(1)
						  ->get($this->tables['sale_items']);

		return ($query->num_rows() > 0) ? $query->row() : FALSE;
	}
}
