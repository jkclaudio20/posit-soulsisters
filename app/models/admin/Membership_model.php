<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Membership_model extends CI_Model {
	/**
	 * Holds and array of table names
	 *
	 * @var array $tables
	 */
	public $tables;

	public function __construct()
	{
		parent::__construct();
		$this->tables = $this->config->item('tables', 'pos');
	}

	/**
	 * Get Membership Types
	 *
	 * @param boolean               $active_only
	 * @param boolean               $return_dropdown
	 * @return object|array|boolean
	 * @author Karlo
	 */
	public function get_membership_types($active_only = FALSE, $return_dropdown = FALSE)
	{
		if ($active_only === TRUE) $this->db->where('status', 'active');

		$this->db->order_by('membership_name');

		$query = $this->db->get($this->tables['membership_types']);

		if ($return_dropdown === TRUE)
		{
			$dropdown       = array();
			$dropdown[NULL] = NULL;

			foreach ($query->result() as $row)
				$dropdown[$row->membership_type_id] = $row->membership_name;

			return $dropdown;
		}
		else
		{
			return ($query->num_rows() > 0) ? $query->result() : FALSE;
		}
	}

	/**
	 * Add Membership Type
	 *
	 * @param  array $insert_data
	 * @return boolean
	 * @author Karlo
	 */
	public function add_membership_type(array $insert_data)
	{
		// Check for duplicates
		$query = $this->db->where('membership_name', $insert_data['membership_name'])
						  ->limit(1)
						  ->get($this->tables['membership_types']);

		if ($query->num_rows() > 0)
		{
			$this->ion_auth->set_error($insert_data['membership_name'] . ' already exists. Please choose a different Membership Name.');
			return FALSE;
		}
		else
		{
			if ($this->db->insert($this->tables['membership_types'], $insert_data))
			{
				$this->ion_auth->set_message($insert_data['membership_name'] . ' successfully added to Membership Types.');
				return TRUE;
			}
			else
			{
				$this->ion_auth->set_error('An error has occured while trying to add '.$insert_data['membership_name'].' to Membership Types. Please try again.');
				return FALSE;
			}
		}
	}

	/**
	 * Get Membership Type Data
	 *
	 * @param  int            $membership_type_id
	 * @return object|boolean
	 * @author Karlo
	 */
	public function get_membership_type_data($membership_type_id)
	{
		$query = $this->db->join($this->tables['users'], $this->tables['membership_types'].'.created_by = '.$this->tables['users'].'.id', 'LEFT')
						  ->where($this->tables['membership_types'].'.membership_type_id', $membership_type_id)
						  ->limit(1)
						  ->get($this->tables['membership_types']);

		return ($query->num_rows() > 0) ? $query->row() : FALSE;
	}

	/**
	 * Update Membership Type
	 *
	 * @param  int     $membership_type_id
	 * @param  array   $update_data
	 * @return boolean
	 * @author Karlo
	 */
	public function update_membership_type($membership_type_id, array $update_data)
	{
		// Check for duplicates
		$query = $this->db->where('membership_name', $update_data['membership_name'])
						  ->where('membership_type_id !=', $membership_type_id)
						  ->limit(1)
						  ->get($this->tables['membership_types']);

		if ($query->num_rows() > 0)
		{
			$this->ion_auth->set_error($update_data['membership_name'] . ' already exists. Please choose another name.');
			return FALSE;
		}
		else
		{
			if ($this->db->update($this->tables['membership_types'], $update_data, array('membership_type_id' => $membership_type_id)))
			{
				$this->ion_auth->set_message($update_data['membership_name'].' successfully updated.');
				return TRUE;
			}
			else
			{
				$this->ion_auth->set_error('An error has occured while trying to update '.$update_data['membership_name'].'. Please try again.');
				return FALSE;
			}
		}
	}

	/**
	 * Delete Membership Type
	 *
	 * @param  int     $membership_type_id
	 * @param  string  $membership_name
	 * @return boolean
	 * @author Karlo
	 */
	public function delete_membership_type($membership_type_id, $membership_name = NULL)
	{
		if ($this->db->delete($this->tables['membership_types'], array('membership_type_id' => $membership_type_id)))
		{
			$message = ( ! is_null($membership_name))
				? $membership_name . ' successfully deleted from Membership Types'
				: 'Membership Type successfully deleted.';

			$this->ion_auth->set_message($message);
			return TRUE;
		}
		else
		{
			$this->ion_auth->set_error('An error has occured while trying to delete Membership Type. Please try again.');
			return FALSE;
		}
	}

	/**
	 * Add Member
	 *
	 * @param  array   $member_data
	 * @param  array   $membership_data
	 * @return boolean
	 * @author Karlo
	 */
	public function add_member(array $member_data, array $membership_data)
	{
		if ( ! empty($member_data) && ! empty($membership_data))
		{
			$user_id = $member_data['member_user_id']; // Get User ID, so we can delete user if necessary

			// Check for complete duplicates (First Name, Middle Name, Last Name, Gender, and Birthdate)
			$filter_duplicate = array(
				'first_name'  => $member_data['first_name'],
				'middle_name' => $member_data['middle_name'],
				'last_name'   => $member_data['last_name'],
				'gender'      => $member_data['gender'],
				'birthdate'   => $member_data['birthdate']
			);

			$query = $this->db->where($filter_duplicate)
							  ->limit(1)
							  ->get($this->tables['members']);

			if ($query->num_rows() > 0)
			{
				// Duplicate Found, Delete User
				if ($this->ion_auth->delete_user($user_id))
				{
					$this->ion_auth->clear_messages();

					$this->ion_auth->set_message('Member data already exists. Unable to add duplicate.');
					return FALSE;
				}
				else
				{
					$this->ion_auth->clear_errors();

					$this->ion_auth->set_message('An error has occured while trying to add new member. Please try again.');
					return FALSE;
				}
			}
			else
			{
				// We're good to go
				$this->db->trans_begin();

				$this->db->insert($this->tables['members'], $member_data);
				$member_id = $this->db->insert_id();
				$membership_data['member_id'] = $member_id; // push member_id to membership_data
				$this->db->insert($this->tables['membership_history'], $membership_data);

				if ($this->db->trans_status() === TRUE)
				{
					$this->db->trans_commit();

					$this->ion_auth->set_message('<strong>'.$member_data['first_name'].' '.$member_data['last_name'].'</strong> successfully added to members.');
					return TRUE;
				}
				else
				{
					$this->db->trans_rollback();

					$this->ion_auth->set_error('An error has occured while trying to add <strong>'.$member_data['first_name'].' '.$member_data['last_name'].'</strong> to members. Please try again.');
					return FALSE;
				}
			}
		}
		else
		{
			// Empty arrays were passed. Bail.
			$this->ion_auth->set_error('Invalid input.');
			return FALSE;
		}
	}

	/**
	 * Get Members
	 *
	 * @return object|boolean
	 * @author Karlo
	 */
	public function get_members()
	{
		$query = $this->db->order_by('first_name')
						  ->get($this->tables['members']);

		return ($query->num_rows() > 0) ? $query->result() : FALSE;
	}

	/**
	 * Get Member Data
	 *
	 * @param  int|null       $member_id
	 * @return object|boolean
	 * @author Karlo
	 */
	public function get_member_data($member_id = NULL)
	{
		$query = $this->db->where('member_id', $member_id)
						  ->limit(1)
						  ->get($this->tables['members']);

		return ($query->num_rows() > 0) ? $query->row() : FALSE;
	}

	/**
	 * Get Membership History
	 *
	 * @param  int|null       $member_id
	 * @return object|boolean
	 * @author Karlo
	 */
	public function get_membership_history($member_id = NULL)
	{
		if ( ! is_null($member_id))
		{
			$query = $this->db->join($this->tables['membership_types'], $this->tables['membership_history'].'.membership_type_id = '.$this->tables['membership_types'].'.membership_type_id', 'LEFT')
							  ->where($this->tables['membership_history'].'.member_id', $member_id)
							  ->order_by($this->tables['membership_history'].'.from', 'DESC')
							  ->get($this->tables['membership_history']);

			return ($query->num_rows() > 0) ? $query->result() : FALSE;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * Delete Member
	 *
	 * @param  int     $member_id
	 * @param  string  $name
	 * @return boolean
	 * @author Karlo
	 */
	public function delete_member($member_id, $name = NULL)
	{
		if ($this->db->delete($this->tables['members'], array('member_id' => $member_id)))
		{
			$message = ( ! is_null($name))
				? $name . ' successfully deleted from Members'
				: 'Member successfully deleted.';

			$this->ion_auth->set_message($message);
			return TRUE;
		}
		else
		{
			$this->ion_auth->set_error('An error has occured while trying to delete Member. Please try again.');
			return FALSE;
		}
	}
}
