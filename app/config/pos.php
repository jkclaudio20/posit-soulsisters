<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Custom Config File
 *
 * TrendMedia Inc.
 * Created : 2018.07.17
 * Author  : Joshua Karlo Claudio
 *           joshua.claudio@trendmedia.ph
 *           jkclaudio20
 */

$config['tables'] = array(
	'membership_types' => 'membership_types',
	'users' => 'users',
	'members' => 'members',
	'membership_history' => 'membership_history',
	'therapists' => 'therapists',
	'sale_items' => 'sale_items',
	'products' => 'products',
	'sales'    => 'sales'
);

$config['member_group_id'] = 6;
