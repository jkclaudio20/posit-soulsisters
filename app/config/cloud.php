<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Custom Config File for Cloud Sync
 *
 * TrendMedia Inc.
 * Created : 2018.08.22
 * Author  : Joshua Karlo Claudio
 *           joshua.claudio@trendmedia.ph
 *           jkclaudio20
 */

$config['transactions'] = array(
	'add_sale' => 'add-sale'
); 

$config['tables'] = array(
	'logs'         => 'logs',
	'sales'        => 'sales',
	'sale_items'   => 'sale_items',
	'sync_history' => 'sync_history',
	'payments'     => 'payments'
);

$config['backup_tables'] = array('sma_sales', 'sma_sale_items', 'sma_payments', 'sma_products', 'sma_warehouse_products');

$config['all_tables'] = array(
	'sma_addresses',
	'sma_adjustment_items',
	'sma_adjustments',
	'sma_brands',
	'sma_calendar',
	'sma_captcha',
	'sma_categories',
	'sma_combo_items',
	'sma_companies',
	'sma_costing',
	'sma_currencies',
	'sma_customer_groups',
	'sma_date_format',
	'sma_deliveries',
	'sma_deposits',
	'sma_expense_categories',
	'sma_expenses',
	'sma_gift_card_topups',
	'sma_gift_cards',
	'sma_groups',
	'sma_login_attempts',
	'sma_logs',
	'sma_migrations',
	'sma_notifications',
	'sma_order_ref',
	'sma_payments',
	'sma_paypal',
	'sma_permissions',
	'sma_pos_register',
	'sma_pos_settings',
	'sma_price_groups',
	'sma_printers',
	'sma_product_photos',
	'sma_product_prices',
	'sma_product_variants',
	'sma_products',
	'sma_purchase_items',
	'sma_purchases',
	'sma_quantity_logs',
	'sma_quote_items',
	'sma_quotes',
	'sma_raw_products',
	'sma_return_items',
	'sma_returns',
	'sma_sale_items',
	'sma_sales',
	'sma_sessions',
	'sma_settings',
	'sma_skrill',
	'sma_stock_count_items',
	'sma_stock_counts',
	'sma_suspended_bills',
	'sma_suspended_items',
	'sma_sync_history',
	'sma_tax_rates',
	'sma_transfer_items',
	'sma_transfers',
	'sma_units',
	'sma_user_logins',
	'sma_users',
	'sma_variants',
	'sma_warehouses',
	'sma_warehouses_products',
	'sma_warehouses_products_variants'
);

$config['mysql_server_path'] = 'C:/xampp/mysql/bin/mysql';