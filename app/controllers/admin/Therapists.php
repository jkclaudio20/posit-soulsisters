<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Therapists extends MY_Controller {
    public $user_id;

    /**
     * @var array $tables
     */
    public $tables = array();

	function __construct()
	{
		parent::__construct();

		if ( ! $this->loggedIn)
		{
			$this->user_id = NULL;

			$this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
		}
		else
		{
			$this->user_id = $this->ion_auth->get_user_id();
		}

		$this->config->load('pos', TRUE);
		$this->load->admin_model('therapists_model');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('', '');

        $this->tables = $this->config->item('tables', 'pos');
	}

    public function index()
    {
        $this->sma->checkPermissions();

        $bc   = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => 'Therapists'));
        $meta = array('page_title' => 'Therapists', 'bc' => $bc);

		$this->data['result']  = $this->session->flashdata('result');
		$this->data['message'] = $this->session->flashdata('message');

		$this->page_construct('therapists/index', $meta, $this->data);
    }

    // Feed Therapists Table with JSON Data from Server
    public function get_therapists()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $this->load->library('datatables');

            if ($this->Owner)
            {
                $actions = '
                    <a class="btn btn-default btn-xs tip" title="Edit" href="'.admin_url('therapists/edit?id=').'$1"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-default btn-xs tip" title="View Therapist Details" href="'.admin_url('therapists/view?id=').'$1"><i class="fa fa-folder-open"></i></a>
                    <a class="btn btn-default btn-xs tip" title="View Services" href="'.admin_url('therapists/services?id=').'$1"><i class="fa fa-briefcase"></i></a>
                    <button type="button" class="btn btn-default btn-xs tip" title="Delete" data-toggle="modal" data-target="#delete-therapist-modal" data-id="$1" data-name="$2"><i class="fa fa-trash-o"></i></button>
                ';
            }
            else
            {
                $actions = '
                    <a class="btn btn-default btn-xs tip" title="Edit" href="'.admin_url('therapists/edit?id=').'$1"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-default btn-xs tip" title="View Therapist Details" href="'.admin_url('therapists/view?id=').'$1"><i class="fa fa-folder-open"></i></a>
                ';

                if ($this->GP['therapists-services']) $actions .= '<a class="btn btn-default btn-xs tip" title="View Services" href="'.admin_url('therapists/services?id=').'$1"><i class="fa fa-briefcase"></i></a>';
                if ($this->GP['therapists-delete']) $actions .= '<button type="button" class="btn btn-default btn-xs tip" title="Delete" data-toggle="modal" data-target="#delete-therapist-modal" data-id="$1" data-name="$2"><i class="fa fa-trash-o"></i></button>';
            }

            $this->datatables->select("therapist_id, CONCAT(first_name, ' ', last_name) AS name, COALESCE(address, '<span class=\"text-muted\">Not available</span>') AS address, COALESCE(mobile_no, '<span class=\"text-muted\">Not available</span>') AS mobile_no")
                             ->from($this->tables['therapists'])
                             ->add_column('actions', $actions, 'therapist_id, name');

            echo $this->datatables->generate();
        }
    }

    // Add Therapist Form
    public function add()
    {
        $this->sma->checkPermissions();

        $bc   = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('therapists'), 'page' => 'Therapists'), array('link' => '#', 'page' => 'Add Therapist'));
        $meta = array('page_title' => 'Add Therapist', 'bc' => $bc);

		$this->data['result']  = $this->session->flashdata('result');
		$this->data['message'] = $this->session->flashdata('message');

        // Form Fields
        $this->data['first_name'] = array(
            'name' => 'first_name',
            'id'   => 'first_name',
            'type' => 'text'
        );

        $this->data['middle_name'] = array(
            'name' => 'middle_name',
            'id'   => 'middle_name',
            'type' => 'text'
        );

        $this->data['last_name'] = array(
            'name' => 'last_name',
            'id'   => 'last_name',
            'type' => 'text'
        );

        $this->data['genders'] = array(
            NULL     => NULL,
            'male'   => 'Male',
            'female' => 'Female'
        );

        $this->data['birthdate'] = array(
            'name' => 'birthdate',
            'id'   => 'birthdate',
            'type' => 'text'
        );

        $this->data['address'] = array(
            'name' => 'address',
            'id'   => 'address',
            'type' => 'text'
        );

        $this->data['mobile_no'] = array(
            'name' => 'mobile_no',
            'id'   => 'mobile_no',
            'type' => 'text'
        );

        $this->data['email'] = array(
            'name' => 'email',
            'id'   => 'email',
            'type' => 'text'
        );

        $this->data['hire_date'] = array(
            'name' => 'hire_date',
            'id'   => 'hire_date',
            'type' => 'text'
        );

		$this->page_construct('therapists/add', $meta, $this->data);
    }

    // Add Therapist
    public function ajax_add()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('middle_name', 'Middle Name', 'trim');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            $this->form_validation->set_rules('birthdate', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('hire_date', 'Hire Date', 'trim');
            $this->form_validation->set_rules('address', 'Address', 'trim');
            $this->form_validation->set_rules('mobile_no', 'Mobile No.', 'trim');
            $this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email');

            if ($this->form_validation->run())
            {
                $birthdate = new DateTime($this->input->post('birthdate'));
                $hire_date = ($this->input->post('hire_date')) ? new DateTime($this->input->post('hire_date')) : FALSE;
                $now       = new DateTime();

                $therapist_data = array(
                    'first_name'   => $this->input->post('first_name'),
                    'middle_name'  => $this->input->post('middle_name'),
                    'last_name'    => $this->input->post('last_name'),
                    'gender'       => $this->input->post('gender'),
                    'birthdate'    => $birthdate->format('Y-m-d'),
                    'address'      => ($this->input->post('address')) ? $this->input->post('address') : NULL,
                    'mobile_no'    => ($this->input->post('mobile_no')) ? $this->input->post('mobile_no') : NULL,
                    'email'        => ($this->input->post('email')) ? $this->input->post('email') : NULL,
                    'hire_date'    => ($hire_date) ? $hire_date->format('Y-m-d') : NULL,
                    'date_created' => $now->format('Y-m-d H:i:s')
                );

                if ($therapist_id = $this->therapists_model->add_therapist($therapist_data))
                {
                    $this->session->set_flashdata('result', 'success');
                    $this->session->set_flashdata('message', $this->ion_auth->messages());

                    $output['result'] = 'success';
                    $output['url']    = admin_url('therapists/view?id='.$therapist_id);
                }
                else
                {
                    $output['result']  = 'error';
                    $output['message'] = $this->ion_auth->errors();
                }
            }
            else
            {
                $output['result']  = 'error';
                $output['message'] = validation_errors();
            }

            echo json_encode($output);
            exit();
        }
    }

    // View Therapist Data
    public function view()
    {
        $this->sma->checkPermissions('index');

        $bc   = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('therapists'), 'page' => 'Therapists'), array('link' => '#', 'page' => 'Therapist Details'));
        $meta = array('page_title' => 'Therapist Details', 'bc' => $bc);

		$this->data['result']  = $this->session->flashdata('result');
		$this->data['message'] = $this->session->flashdata('message');

        $therapist_id = $this->input->get('id');

        $this->data['row'] = $this->therapists_model->get_therapist_data($therapist_id);

		$this->page_construct('therapists/view', $meta, $this->data);
    }

    // Edit Therapist Form
    public function edit()
    {
        $this->sma->checkPermissions();

        $bc   = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('therapists'), 'page' => 'Therapists'), array('link' => '#', 'page' => 'Edit Therapist'));
        $meta = array('page_title' => 'Edit Therapist', 'bc' => $bc);

		$this->data['result']  = $this->session->flashdata('result');
		$this->data['message'] = $this->session->flashdata('message');

        $therapist_id = $this->input->get('id');

        $this->data['row'] = $this->therapists_model->get_therapist_data($therapist_id);

        if ($this->data['row'])
        {
            // Form Fields
            $this->data['first_name'] = array(
                'name' => 'first_name',
                'id'   => 'first_name',
                'type' => 'text'
            );

            $this->data['middle_name'] = array(
                'name' => 'middle_name',
                'id'   => 'middle_name',
                'type' => 'text'
            );

            $this->data['last_name'] = array(
                'name' => 'last_name',
                'id'   => 'last_name',
                'type' => 'text'
            );

            $this->data['genders'] = array(
                NULL     => NULL,
                'male'   => 'Male',
                'female' => 'Female'
            );

            $this->data['birthdate'] = array(
                'name' => 'birthdate',
                'id'   => 'birthdate',
                'type' => 'text'
            );

            $this->data['address'] = array(
                'name' => 'address',
                'id'   => 'address',
                'type' => 'text'
            );

            $this->data['mobile_no'] = array(
                'name' => 'mobile_no',
                'id'   => 'mobile_no',
                'type' => 'text'
            );

            $this->data['email'] = array(
                'name' => 'email',
                'id'   => 'email',
                'type' => 'text'
            );

            $this->data['hire_date'] = array(
                'name' => 'hire_date',
                'id'   => 'hire_date',
                'type' => 'text'
            );
        }

		$this->page_construct('therapists/edit', $meta, $this->data);
    }

    // Update Therapist
    public function ajax_edit()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
            $this->form_validation->set_rules('middle_name', 'Middle Name', 'trim');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('gender', 'Gender', 'required');
            $this->form_validation->set_rules('birthdate', 'Date of Birth', 'trim|required');
            $this->form_validation->set_rules('hire_date', 'Hire Date', 'trim');
            $this->form_validation->set_rules('address', 'Address', 'trim');
            $this->form_validation->set_rules('mobile_no', 'Mobile No.', 'trim');
            $this->form_validation->set_rules('email', 'Email Address', 'trim|valid_email');

            if ($this->form_validation->run())
            {
                $birthdate = new DateTime($this->input->post('birthdate'));
                $hire_date = ($this->input->post('hire_date')) ? new DateTime($this->input->post('hire_date')) : FALSE;

                $therapist_id = $this->input->post('therapist_id');

                $therapist_data = array(
                    'first_name'   => $this->input->post('first_name'),
                    'middle_name'  => $this->input->post('middle_name'),
                    'last_name'    => $this->input->post('last_name'),
                    'gender'       => $this->input->post('gender'),
                    'birthdate'    => $birthdate->format('Y-m-d'),
                    'address'      => ($this->input->post('address')) ? $this->input->post('address') : NULL,
                    'mobile_no'    => ($this->input->post('mobile_no')) ? $this->input->post('mobile_no') : NULL,
                    'email'        => ($this->input->post('email')) ? $this->input->post('email') : NULL,
                    'hire_date'    => $hire_date->format('Y-m-d')
                );

                if ($this->therapists_model->update_therapist($therapist_id, $therapist_data))
                {
                    $this->session->set_flashdata('result', 'success');
                    $this->session->set_flashdata('message', $this->ion_auth->messages());

                    $output['result'] = 'success';
                    $output['url']    = admin_url('therapists/view?id='.$therapist_id);
                }
                else
                {
                    $output['result']  = 'error';
                    $output['message'] = $this->ion_auth->errors();
                }
            }
            else
            {
                $output['result']  = 'error';
                $output['message'] = validation_errors();
            }

            echo json_encode($output);
            exit();
        }
    }

    // Delete Therapist
    public function delete()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed');
        }
        else
        {
            $this->sma->checkPermissions('delete');

            $therapist_id = $this->input->post('id');

            if ($this->therapists_model->delete_therapist($therapist_id))
            {
                $output['result']  = 'success';
                $output['message'] = $this->ion_auth->messages();
            }
            else
            {
                $output['result']  = 'error';
                $output['message'] = $this->ion_auth->errors();
            }

            echo json_encode($output);
            exit();
        }
    }

    // View Services
    public function services()
    {
        $this->sma->checkPermissions();

        $bc   = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('therapists'), 'page' => 'Therapists'), array('link' => '#', 'page' => 'Therapist Details'));
        $meta = array('page_title' => 'Therapist Details', 'bc' => $bc);

		$this->data['result']  = $this->session->flashdata('result');
		$this->data['message'] = $this->session->flashdata('message');

        $therapist_id = $this->input->get('id');
        $this->data['row'] = $this->therapists_model->get_therapist_data($therapist_id);
        $this->data['therapist_id'] = $therapist_id;

        $this->data['therapists'] = $this->therapists_model->get_therapists(TRUE);

        $this->data['from'] = array(
            'name' => 'from',
            'id'   => 'from',
            'type' => 'text'
        );

        $this->data['to'] = array(
            'name' => 'to',
            'id'   => 'to',
            'type' => 'text'
        );

		$this->page_construct('therapists/services', $meta, $this->data);
    }

    // Feed Services Table with JSON Data from Server
    public function get_services()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $this->load->library('datatables');

            $therapist_id = $this->input->get('id');
            $from         = ($this->input->get('from')) ? new DateTime($this->input->get('from')) : FALSE;
            $to           = ($this->input->get('to')) ? new DateTime($this->input->get('to')) : FALSE;

            $this->datatables->select($this->tables['sale_items'].".id, product_name, ".$this->tables['sales'].".date, COALESCE(subtotal, 0), COALESCE(incentive_amount, 0), COALESCE((subtotal * incentive_amount), 0) AS incentive")
                             ->from($this->tables['sale_items'])
                             ->join($this->tables['products'], $this->tables['sale_items'].'.product_id = '.$this->tables['products'].'.id')
                             ->join($this->tables['sales'], $this->tables['sale_items'].'.sale_id = '.$this->tables['sales'].'.id')
                             ->where($this->tables['sale_items'].'.therapist_id IS NOT NULL')
                             ->where($this->tables['sale_items'].'.therapist_id', $therapist_id);

            if ($from && $to)
            {
                $this->datatables->where($this->tables['sales'].'.date >=', $from->format('Y-m-d 00:00:00'))
                                 ->where($this->tables['sales'].'.date <=', $to->format('Y-m-d 23:59:59'));
            }

            echo $this->datatables->generate();
        }
    }

    // Bulk Actions
    public function bulk_actions()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == TRUE)
        {
            if (!empty($_POST['val']))
            {
                if ($this->input->post('form_action') == 'export_excel')
                {
                    $filename = 'therapists_list_'.time();

                    $this->load->library('excel');

                    $this->excel->setActiveSheetIndex(0);

                    $sheet = $this->excel->getActiveSheet();
                    $sheet->setTitle('Therapist Incentive');

                    $sheet->setCellValue('A1', 'Therapists List');
                    $sheet->mergeCells('A1:J1');

                    $sheet->setCellValue('B2', 'First Name');
                    $sheet->setCellValue('C2', 'Middle Name');
                    $sheet->setCellValue('D2', 'Last Name');
                    $sheet->setCellValue('E2', 'Gender');
                    $sheet->setCellValue('F2', 'Date of Birth');
                    $sheet->setCellValue('G2', 'Address');
                    $sheet->setCellValue('H2', 'Mobile No.');
                    $sheet->setCellValue('I2', 'Email Address');
                    $sheet->setCellValue('J2', 'Hire Date');

                    $sheet->getStyle('A2:J2')->getFont()->setBold(TRUE);

                    $i = 1;
                    $y = 3;

                    foreach ($_POST['val'] as $id)
                    {
                        $row = $this->therapists_model->get_therapist_data($id);

                        if ($row)
                        {
                            $sheet->setCellValue('A'.$y, $i++);
                            $sheet->setCellValue('B'.$y, $row->first_name);
                            $sheet->setCellValue('C'.$y, $row->middle_name);
                            $sheet->setCellValue('D'.$y, $row->last_name);
                            $sheet->setCellValue('E'.$y, ucfirst($row->gender));
                            $bdate = new DateTime($row->birthdate);
                            $sheet->setCellValue('F'.$y, $bdate->format('F j, Y'));
                            $sheet->setCellValue('G'.$y, $row->address);
                            $sheet->setCellValue('H'.$y, $row->mobile_no);
                            $sheet->setCellValue('I'.$y, $row->email);
                            $hdate = ($row->hire_date) ? new DateTime($row->hire_date) : FALSE;
                            $sheet->setCellValue('J'.$y, ($hdate) ? $hdate->format('F j, Y') : NULL);
                            $y++;
                        }
                    }

                    $sheet->getColumnDimension('A')->setWidth(5);
                    $sheet->getColumnDimension('B')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('C')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('D')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('E')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('F')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('G')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('H')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('I')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('J')->setAutoSize(TRUE);

                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            }
            else
            {
                //$this->session->set_flashdata('result','error');
                $this->session->set_flashdata('error', 'None selected');
                redirect($_SERVER["HTTP_REFERER"]);
            }
        }
        else
        {
            $this->session->set_flashdata('result', 'error');
            $this->session->set_flashdata('message', validation_errors());
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'admin/therapists/services');
        }
    }

    // Services Bulk Actions
    public function services_actions()
    {
        $this->sma->checkPermissions('bulk_actions');

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == TRUE)
        {
            if (!empty($_POST['val']))
            {
                if ($this->input->post('form_action') == 'export_excel')
                {
                    $therapist_id = $this->input->post('therapist_id');

                    $therapist = $this->therapists_model->get_therapist_data($therapist_id);
                    $t_name = ($therapist) ? "{$therapist->first_name} {$therapist->last_name}" : 'Unknown Therapist';

                    $filename = 'Incentive - '.$t_name.' '.time();

                    $this->load->library('excel');

                    $this->excel->setActiveSheetIndex(0);

                    $sheet = $this->excel->getActiveSheet();
                    $sheet->setTitle('Therapist Incentive');

                    $sheet->setCellValue('A1', $t_name);
                    $sheet->mergeCells('A1:F1');
                    $sheet->setCellValue('B2', 'Service');
                    $sheet->setCellValue('C2', 'Date');
                    $sheet->setCellValue('D2', 'Price');
                    $sheet->setCellValue('E2', 'Incentive Percentage');
                    $sheet->setCellValue('F2', 'Incentive');

                    $sheet->getStyle('A2:F2')->getFont()->setBold(TRUE);

                    $i = 1;
                    $y = 3;

                    foreach ($_POST['val'] as $id)
                    {
                        $row = $this->therapists_model->get_service_data($id);

                        if ($row)
                        {
                            $sheet->setCellValue('A'.$y, $i++);
                            $sheet->setCellValue('B'.$y, $row->product_name);

                            $date = new DateTime($row->date);
                            $sheet->setCellValue('C'.$y, $date->format('m/d/Y h:i A'));

                            $sheet->setCellValue('D'.$y, $row->subtotal);
                            $sheet->setCellValue('E'.$y, number_format($row->incentive_amount * 100) . '%');
                            $sheet->setCellValue('F'.$y, $row->incentive);

                            $sheet->getStyle('D'.$y)->getNumberFormat()->setFormatCode('#,##0.00');
                            $sheet->getStyle('F'.$y)->getNumberFormat()->setFormatCode('#,##0.00');

                            $y++;
                        }
                    }

                    $less_y = $y - 1;
                    $sheet->setCellValue('E'.$y, 'Total Incentive:');
                    $sheet->setCellValue('F'.$y, '=SUM(F3:F'.$less_y.')');
                    $sheet->getStyle('F'.$y)->getNumberFormat()->setFormatCode('#,##0.00');
                    $sheet->getStyle('F'.$y)->getFont()->setBold(TRUE);

                    $sheet->getColumnDimension('A')->setWidth(5);
                    $sheet->getColumnDimension('B')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('C')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('D')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('E')->setAutoSize(TRUE);
                    $sheet->getColumnDimension('F')->setAutoSize(TRUE);

                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            }
            else
            {
                //$this->session->set_flashdata('result','error');
                $this->session->set_flashdata('error', 'None selected');
                redirect($_SERVER["HTTP_REFERER"]);
            }
        }
        else
        {
            $this->session->set_flashdata('result', 'error');
            $this->session->set_flashdata('message', validation_errors());
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'admin/therapists/services');
        }
    }
}
