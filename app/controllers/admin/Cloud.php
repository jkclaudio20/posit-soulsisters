<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cloud extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->admin_model('cloud_model');
    }

    function index()
    {
        echo 'Welcome to cloud DB synchronizer.';
    }

    function run()
    {
    	if ( ! $this->input->is_cli_request())
		{
			exit('No direct script access allowed.');
		}
		else
		{
			$from = ($this->input->get('from')) ? $this->input->get('from') : FALSE;
	    	$to   = ($this->input->get('to'))   ? $this->input->get('to')   : FALSE;

	    	$this->cloud_model->sync_db($from, $to);
	    	echo "Exiting synchronizer...";
	    	exit();
		}
    }

    function status()
    {
        if ( ! $this->input->is_cli_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $this->cloud_model->get_sync_status();
            exit();
        }
    }

    function csv()
    {
        if ( ! $this->input->is_cli_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            echo $this->cloud_model->csv();
            exit();
        }
    }

    function backup_db()
    {
        if ( ! $this->input->is_cli_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $this->cloud_model->backup_db('cloud');
            exit();
        }
    }

    function fix_db()
    {
        if ( ! $this->input->is_cli_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $now = new DateTime();

            echo "[{$now->format('Y-m-d H:i:s')}] Fetching cloud database...".PHP_EOL;
            $this->cloud_model->fix_db();
            exit();
        }
    }
}
