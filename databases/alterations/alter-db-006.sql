-- 2019.02.11
ALTER TABLE `posit_soulsisters`.`sma_permissions` ADD COLUMN `therapists-index` TINYINT(1) DEFAULT 0 NULL AFTER `reports-best_sellers`, ADD COLUMN `therapists-add` TINYINT(1) DEFAULT 0 NULL AFTER `therapists-index`, ADD COLUMN `therapists-edit` TINYINT(1) DEFAULT 0 NULL AFTER `therapists-add`, ADD COLUMN `therapists-delete` TINYINT(1) DEFAULT 0 NULL AFTER `therapists-edit`;
ALTER TABLE `posit_soulsisters`.`sma_permissions` ADD COLUMN `therapists-bulk_actions` TINYINT(1) DEFAULT 0 NULL AFTER `therapists-delete`;
CREATE TABLE `posit_soulsisters`.`sma_therapists`( `therapist_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, `first_name` VARCHAR(20) NOT NULL, `middle_name` VARCHAR(20), `last_name` VARCHAR(20) NOT NULL, `gender` ENUM('male','female'), `birthdate` DATE, `address` VARCHAR(100), `mobile_no` VARCHAR(20), `email` VARCHAR(50), `hire_date` DATE, `date_created` DATETIME NOT NULL, PRIMARY KEY (`therapist_id`) );

-- 2019.02.12
ALTER TABLE `posit_soulsisters`.`sma_products` ADD COLUMN `with_therapist` TINYINT(1) NULL AFTER `second_name`, ADD COLUMN `incentive_amount` DECIMAL(10,4) NULL AFTER `with_therapist`;
ALTER TABLE `posit_soulsisters`.`sma_sale_items` ADD COLUMN `therapist_id` INT(11) NULL AFTER `comment`;
ALTER TABLE `posit_soulsisters`.`sma_permissions` ADD COLUMN `therapists-services` TINYINT(1) DEFAULT 0 NULL AFTER `therapists-bulk_actions`;
