-- 2019.06.05
ALTER TABLE `posit_soulsisters`.`sma_settings` ADD COLUMN `expiry_date` DATE NULL AFTER `pdf_lib`, ADD COLUMN `expired` TINYINT(1) DEFAULT 0 NOT NULL AFTER `expiry_date`;
