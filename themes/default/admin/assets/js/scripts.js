/**
 * Scripts JS
 *
 * Created: 2018.08.17
 * Author : TrendMedia Inc. | Karlo
 *          joshua.claudio@trendmedia.ph
 */

 jQuery(document).ready(function(){
 	// Constants
 	const _MODAL_LOADING = '<h3 class="text-center text-muted"><i class="fa fa-refresh fa-spin"></i> Loading...</h3>';
 	const _JSON = 'JSON';
    const _HTML = 'HTML';
 	const _MODAL_ERR_MSG = '<h3 class="text-center"><i class="fa fa-warning"></i> Something went wrong. Please check your internet connection and try again.</h3>';
 	const _BTN_SAVING = 'Saving...';
 	const _BTN_UPDATING = 'Updating...';
 	const _BTN_DELETING = 'Deleting...';
    const _BTN_ACTIVATING = 'Activating...';
    const _BTN_DEACTIVATING = 'Deactivating...';
 	const _BTN_WAIT = 'Please wait...';
    const _BTN_CLOCKING_IN = '<i class="fa fa-refresh fa-spin"></i> Clocking in...';
    const _BTN_CLOCKING_OUT = '<i class="fa fa-refresh fa-spin"></i> Clocking out...';
    const _BTN_CLOCK_IN = '<i class="fa fa-sign-in"></i> Clock in';
    const _BTN_CLOCK_OUT = '<i class="fa fa-sign-out"></i> Clock out';
 	const _POST = 'POST';
 	const _NOTIF_ERR_MSG = {title : 'Error', text : 'Something went wrong. Please check your internet connection and try again.', type : 'error'};

 	/**
 	 * Add Notification
 	 *
 	 * @param object  config
 	 * @param boolean clear
 	 * @return PNotify Instance
 	 * @author Karlo
 	 */
 	function add_notification(config = {title: 'Message', text: 'Crossfit Cabalen', type: 'info'}, clear = true)
 	{
 		if (clear !== false)
 		{
 			PNotify.removeAll();
 		}

 		return new PNotify(config);
 	}

 	/**
 	 * Refresh table
 	 *
 	 * @param  string table_name
 	 * @return boolean
 	 * @author Karlo
 	 */
 	function refresh_table(table_name = 'table')
 	{
 		var _trigger = jQuery('[name=refresh_'+table_name+']');

 		if (_trigger.length != 0)
 		{
 			_trigger.val(1)
 				    .trigger('change');

 			console.log('Table: '+table_name+' data reloaded via AJAX.'); // for dev, comment this out for production
 			return true;
 		}

 		console.log('Unable to refresh table, '+table_name+' does not exist in DOM.'); // for dev, comment this out for production
 		return false;
 	}

    // Add New Therapist
    jQuery(document).on('submit', '#add-therapist-form', function(e){
        e.preventDefault();

        var form = jQuery(this);
        var btn  = form.find('[type=submit]');
        var dtxt = btn.html();

        if (form.parsley('isValid'))
        {
            btn.html(_BTN_SAVING).prop('disabled', true);

            var form_data = new FormData(this);

            jQuery.ajax({
                url         : form.attr('action'),
                type        : _POST,
                dataType    : _JSON,
                data        : form_data,
                cache       : false,
                contentType : false,
                processData : false,
                success     : function(data)
                {
                    btn.html(dtxt).prop('disabled', false);

                    if (data.result == 'success')
                    {
                        add_notification({
                            title : 'Redirecting',
                            text  : 'Please wait...',
                            type  : 'info'
                        });

                        window.location.href = data.url;
                    }
                    else
                    {
                        add_notification({
                            title : 'Error',
                            text  : data.message,
                            type  : 'error'
                        });
                    }
                },
                error       : function()
                {
                    btn.html(dtxt).prop('disabled', false);
                    add_notification(_NOTIF_ERR_MSG);
                }
            });
        }
    });

    // Update Therapist
    jQuery(document).on('submit', '#edit-therapist-form', function(e){
        e.preventDefault();

        var form = jQuery(this);
        var btn  = form.find('[type=submit]');
        var dtxt = btn.html();

        if (form.parsley('isValid'))
        {
            btn.html(_BTN_SAVING).prop('disabled', true);

            var form_data = new FormData(this);

            jQuery.ajax({
                url         : form.attr('action'),
                type        : _POST,
                dataType    : _JSON,
                data        : form_data,
                cache       : false,
                contentType : false,
                processData : false,
                success     : function(data)
                {
                    btn.html(dtxt).prop('disabled', false);

                    if (data.result == 'success')
                    {
                        add_notification({
                            title : 'Redirecting',
                            text  : 'Please wait...',
                            type  : 'info'
                        });

                        window.location.href = data.url;
                    }
                    else
                    {
                        add_notification({
                            title : 'Error',
                            text  : data.message,
                            type  : 'error'
                        });
                    }
                },
                error       : function()
                {
                    btn.html(dtxt).prop('disabled', false);
                    add_notification(_NOTIF_ERR_MSG);
                }
            });
        }
    });

    // Show Delete Therapist Confirmation Box
    jQuery(document).on('show.bs.modal', '#delete-therapist-modal', function(e){
        var modal = jQuery(this);

        modal.find('#name').html(e.relatedTarget.dataset.name);
        modal.find('.btn-confirm').data('id', e.relatedTarget.dataset.id);
    });

    // Delete Therapist Confirmed
    jQuery(document).on('click', '#delete-therapist-modal .btn-confirm', function(){
        var btn   = jQuery(this);
        var dtxt  = btn.html();
        var modal = btn.parents('.modal');

        btn.html(_BTN_DELETING).prop('disabled', true);

        jQuery.ajax({
            url      : btn.data('url'),
            type     : _POST,
            dataType : _JSON,
            data     : {id : btn.data('id')},
            success  : function(data)
            {
                btn.html(dtxt).prop('disabled', false);

                if (data.result == 'success')
                {
                    refresh_table('therapists');
                    modal.modal('hide');
                    add_notification({
                        title : 'Success',
                        text  : data.message,
                        type  : 'success'
                    });
                }
                else
                {
                    add_notification({
                        title : 'Error',
                        text  : data.message,
                        type  : 'error'
                    });
                }
            },
            error    : function()
            {
                btn.html(dtxt).prop('disabled', false);
                add_notification(_NOTIF_ERR_MSG);
            }
        });
    });
 });
