<?php if ($Owner || $GP['therapists-bulk_actions']): ?>
    <?= admin_form_open('therapists/services_actions', 'id="action-form"') ?>
<?php endif; ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-briefcase"></i>Services</h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-ellipsis-v tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo admin_url('therapists/add'); ?>"><i class="fa fa-plus"></i> Add Therapist</a>
                        </li>
                        <?php if ($Owner || $GP['therapists-bulk_actions']): ?>
                            <li class="divider"></li>
                            <li><a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12" id="services_body">

                <?php if ($row): ?>
                    <p class="introtext">
                        <?php echo "{$row->first_name} {$row->last_name}"; ?>
                    </p>
                <?php endif; ?>

                <button type="button" class="btn btn-default btn-sm" id="btn-toggle-form">Show Form</button>

                <div id="subform" class="row" data-shown="0" style="display: none;">
                    <div class="col-xs-12 col-md-5">
                        <div class="form-group">
                            <label for="therapist_id">Therapist:</label>
                            <?php echo form_dropdown('therapist_id', $therapists, NULL, 'class="form-control select" data-placeholder="Therapist" id="therapist_id"'); ?>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-7">
                        <div class="form-group">
                            <label for="from">Date Range:</label>
                            <div class="input-group">
                                <?php echo form_input($from, '', 'class="form-control date" placeholder="From" autocomplete="off"'); ?>
                                <span class="input-group-addon">to</span>
                                <?php echo form_input($to, '', 'class="form-control date" placeholder="To" autocomplete="off"'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="btn-generate-incentive">Submit</button>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="ln_solid"></div>
                </div>

                <?= form_hidden('refresh_services', 0); ?>
                <table class="table table-bordered table-hover table-striped table-condensed" id="services-table">
                    <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth" type="checkbox" name="check"/>
                            </th>
                            <th>Service</th>
                            <th>Date</th>
                            <th width="20%">Price</th>
                            <th width="20%">Incentive Percentage</th>
                            <th width="20%">Incentive</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                    </tbody>
                    <tfoot class="dtFilter">
                    <tr class="active">
                        <th style="min-width:30px; width: 30px; text-align: center;">
                            <input class="checkbox checkft" type="checkbox" name="check"/>
                        </th>
                        <th></th>
                        <th></th>
                        <th>Price</th>
                        <th>Total Incentive</th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['therapists-bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('perform_action', 'perform_action', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script type="text/javascript">
    jQuery(document).ready(function(){
        var url = ('<?php echo $therapist_id; ?>' == '') ? '<?= admin_url('therapists/get_services') ?>' : '<?= admin_url('therapists/get_services?id='.$therapist_id) ?>'
        var table = $('#services-table').dataTable({
            "aaSorting": [[2, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': url,
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{
                "bSortable": false,
                "mRender": checkbox
            }, null, {mRender: fld}, {mRender: currencyFormat}, {mRender: percentage}, {mRender: currencyFormat}],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var total_incentive = 0;
                for (var i = 0; i < aaData.length; i++) {
                    total_incentive += parseFloat(aaData[aiDisplay[i]][5]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[5].innerHTML = currencyFormat(parseFloat(total_incentive));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[Service]", filter_type: "text", data: []},
        ], "footer");

        jQuery(document).on('change', '[name=refresh_services]', function(){
            if (jQuery(this).val() == 1) table.fnReloadAjax();
            jQuery(this).val(0);
        });

        jQuery(document).on('click', '#btn-toggle-form', function(){
            var form = jQuery('#subform');
            var btn  = jQuery(this);

            if (form.data('shown') == 1)
            {
                form.slideUp();
                form.data('shown', 0);
                btn.html('Show Form');
            }
            else
            {
                form.slideDown();
                form.data('shown', 1);
                btn.html('Hide Form');
            }
        });

        jQuery(document).on('click', '#btn-generate-incentive', function(){
            var therapist_id = jQuery('#therapist_id').val();
            var from         = jQuery('#from').val();
            var to           = jQuery('#to').val();

            table.fnReloadAjax('<?= admin_url('therapists/get_services') ?>'+'?id='+therapist_id+'&from='+from+'&to='+to);

            var name = jQuery('option[value='+therapist_id+']').html();

            if (name)
            {
                if (jQuery('p.introtext').length == 0)
                {
                    jQuery('#services_body').prepend('<p class="introtext">'+name+'</p>')
                }
                else
                {
                    jQuery('p.introtext').html(name);
                }
            }
            else
            {
                jQuery('p.introtext').remove();
            }

            jQuery('#btn-toggle-form').trigger('click');
        });

        if ('<?php echo $therapist_id; ?>' == '')
        {
            jQuery('#btn-toggle-form').trigger('click');
        }
        else
        {
            jQuery('#therapist_id').val('<?php echo $therapist_id; ?>');
        }
    });
</script>
