<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-user-plus"></i><?php echo ($row) ? "{$row->first_name} {$row->last_name} (Therapist Details)" : 'Therapist Details'; ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-ellipsis-v tip" data-placement="left" title="Options"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <?php if ($row): ?>
                            <li>
                                <a href="<?php echo admin_url('therapists/edit?id='.$row->therapist_id); ?>"><i class="fa fa-pencil"></i> Edit Therapist</a>
                            </li>
                            <li class="divider"></li>
                        <?php endif; ?>
                        <li>
                            <a href="<?php echo admin_url('therapists'); ?>"><i class="fa fa-users"></i> Therapists List</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <?php if ($row): ?>
                    <form class="form-horizontal form-label-left">
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 col-md-3">Name:</label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static">
                                    <?php echo "{$row->first_name} {$row->middle_name} {$row->last_name}"; ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 col-md-3">Gender:</label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static">
                                    <?php echo '<i class="fa fa-'.$row->gender.'"></i> '.ucfirst($row->gender); ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 col-md-3">Date of Birth:</label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static">
                                    <?php
                                        $birthdate = new DateTime($row->birthdate);

                                        echo $birthdate->format('F j, Y');
                                    ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 col-md-3">Address:</label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static">
                                    <?php echo ($row->address) ? $row->address : '<span class="text-muted">Not Available</span>'; ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 col-md-3">Mobile No.:</label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static">
                                    <?php echo ($row->mobile_no) ? $row->mobile_no : '<span class="text-muted">Not Available</span>'; ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 col-md-3">Email Address:</label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static">
                                    <?php echo ($row->email) ? $row->email : '<span class="text-muted">Not Available</span>'; ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 col-md-3">Hire Date:</label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static">
                                    <?php
                                        $hire_date = ($row->hire_date) ? new DateTime($row->hire_date) : FALSE;

                                        echo ($hire_date) ? $hire_date->format('F j, Y') : '<span class="text-muted">Not Available</span>';
                                    ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-3 col-md-3">Date Created:</label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static">
                                    <?php
                                        $date_created = new DateTime($row->date_created);

                                        echo $date_created->format('F j, Y h:i A');
                                    ?>
                                </p>
                            </div>
                        </div>
                    </form>
                <?php else: ?>
                    <h3><i class="fa fa-warning"></i> Therapist not found</h3>
                    <a href="<?php echo admin_url('therapists'); ?>">Go back to Therapists List</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
