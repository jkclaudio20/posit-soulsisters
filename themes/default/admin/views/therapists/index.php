<?php if ($Owner || $GP['therapists-bulk_actions']): ?>
    <?= admin_form_open('therapists/bulk_actions', 'id="action-form"') ?>
<?php endif; ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i>Therapists</h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-ellipsis-v tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo admin_url('therapists/add'); ?>"><i class="fa fa-plus"></i> Add Therapist</a>
                        </li>
                        <?php if ($Owner || $GP['therapists-bulk_actions']): ?>
                            <li class="divider"></li>
                            <li><a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <?= form_hidden('refresh_therapists', 0); ?>
                <table class="table table-bordered table-hover table-striped table-condensed" id="therapists-table">
                    <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth" type="checkbox" name="check"/>
                            </th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Mobile No.</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                    </tbody>
                    <tfoot class="dtFilter">
                    <tr class="active">
                        <th style="min-width:30px; width: 30px; text-align: center;">
                            <input class="checkbox checkft" type="checkbox" name="check"/>
                        </th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="width:180px;"><?= lang("actions"); ?></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['therapists-bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('perform_action', 'perform_action', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<!-- MODALS -->
<div id="delete-therapist-modal" class="modal fade" data-backdrop="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="fa fa-question-circle"></i> Delete Confirmation</h5>
			</div>
			<div class="modal-body">
				You are about to delete <span class="text-bold" id="name">this</span> from Therapists. This action is irreversible. Do you want to proceed?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger btn-confirm" data-url="<?= admin_url('therapists/delete') ?>">Delete</button>
			</div>
		</div>
	</div>
</div>
<!-- /MODALS -->

<script type="text/javascript">
    jQuery(document).ready(function(){
        var table = $('#therapists-table').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('therapists/get_therapists') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{
                "bSortable": false,
                "mRender": checkbox
            }, null, null, null, {"bSortable": false, "sClass": 'text-center'}]
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[Name]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[Address]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[Mobile No.]", filter_type: "text", data: []},
        ], "footer");

        jQuery(document).on('change', '[name=refresh_therapists]', function(){
            if (jQuery(this).val() == 1) table.fnReloadAjax();
            jQuery(this).val(0);
        });
    });
</script>
