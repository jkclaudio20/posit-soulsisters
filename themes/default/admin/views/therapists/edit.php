<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-user-plus"></i>Edit Therapist</h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-ellipsis-v tip" data-placement="left" title="Options"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo admin_url('therapists'); ?>"><i class="fa fa-users"></i> Therapists List</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <?php if ($row): ?>
                    <p class="introtext"><?php echo lang('update_info'); ?></p>

                    <?php echo admin_form_open('therapists/ajax_edit', 'data-parsley-validate class="form-horizontal form-label-left" id="edit-therapist-form"'); ?>
                        <?php echo form_hidden('therapist_id', $row->therapist_id); ?>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-2 col-md-2">Name *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <?php echo form_input($first_name, $row->first_name, 'class="form-control" placeholder="First Name *" required'); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <?php echo form_input($middle_name, $row->middle_name, 'class="form-control" placeholder="Middle Name"'); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <?php echo form_input($last_name, $row->last_name, 'class="form-control" placeholder="Last Name *" required'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-2 col-md-2">Gender *</label>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <?php echo form_dropdown('gender', $genders, $row->gender, 'class="form-control" id="gender" data-placeholder="Gender" required'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-2 col-md-2">Date of Birth *</label>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <?php
                                    $bdate = new DateTime($row->birthdate);
                                    echo form_input($birthdate, $bdate->format('m/d/Y'), 'class="form-control date" placeholder="Date of Birth" autocomplete="off" required');
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-2 col-md-2">Hire Date</label>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                <?php
                                    $hdate = ($row->hire_date) ? new DateTime($row->hire_date) : FALSE;
                                    echo form_input($hire_date, ($hdate) ? $hdate->format('m/d/Y') : NULL, 'class="form-control date" placeholder="Hire Date" autocomplete="off"');
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-2 col-md-2">Address</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <?php echo form_input($address, $row->address, 'class="form-control" placeholder="Address"'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-2 col-md-2">Mobile No.</label>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <?php echo form_input($mobile_no, $row->mobile_no, 'class="form-control" placeholder="Mobile No."'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-2 col-md-2">Email Address</label>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <?php echo form_input($email, '', 'class="form-control" placeholder="Email Address"'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 col-sm-10 col-md-10 col-sm-offset-2 col-md-offset-2">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <button type="reset" class="btn btn-danger">Reset</button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                <?php else: ?>
                    <h3><i class="fa fa-warning"></i> Therapist not found</h3>
                    <a href="<?php echo admin_url('therapists'); ?>">Go back to Therapists List</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
