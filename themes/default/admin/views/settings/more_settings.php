<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#btn-preview-login').attr('disabled', true);
        var color = jQuery('#get').val();
        var text = jQuery('#put').val();

        var isColorValidHex = /^#[0-9A-F]{6}$/i.test(color);
        var isTextValidHex = /^#[0-9A-F]{6}$/i.test(text);
        if(isColorValidHex && isTextValidHex)
        {
            jQuery('#btn-preview-login').attr('disabled', false);
        }
        else
        {
            jQuery('#btn-preview-login').attr('disabled', true);
        }
        jQuery('#put').change(function(){
            var color = jQuery(this).val();

            var isValidHex  = /^#[0-9A-F]{6}$/i.test(color);

            if(isValidHex)
            {
                //If valid hex value
                jQuery('#btn-preview-login').attr('disabled', false);
            }
            else
            {
                jQuery('#btn-preview-login').attr('disabled', true);
            }

            jQuery('#get').val(color);
            jQuery('#btn-preview-login').attr('href', '<?= admin_url('system_settings/preview_login/') ?>'+color.substring(1));
        });

        jQuery(document).on('change', '#get', function(){
            var color = jQuery(this).val();

            var isValidHex  = /^#[0-9A-F]{6}$/i.test(color);

            if(isValidHex)
            {
                //If valid hex value
                jQuery('#btn-preview-login').attr('disabled', false);
            }
            else
            {
                jQuery('#btn-preview-login').attr('disabled', true);
            }

            jQuery('#put').val(color);
            jQuery('#btn-preview-login').attr('href', '<?= admin_url('system_settings/preview_login/') ?>'+color.substring(1));
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-wrench"></i><?= lang('more_settings'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('update_info'); ?></p>

                <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open_multipart("system_settings/more_settings", $attrib);
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border"><?= lang('receipt_details') ?></legend>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="checkbox" value="1" name="biller_logo"<?= $show_biller_logo ? "checked" : ''; ?>>
                                    <label>
                                        <?= lang("biller_logo", "biller_logo"); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="checkbox" value="1" name="barcode"<?= $show_barcode ? "checked" : ''; ?>>
                                    <label>
                                        <?= lang("barcode", "barcode"); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="checkbox" value="1" name="qrcode"<?= $show_qrcode ? "checked" : ''; ?>>
                                    <label>
                                        <?= lang("qrcode", "qrcode"); ?>
                                    </label>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border"><?= lang('background_color') ?></legend>
                            <div class="col-md-4">
                                    <label><?= lang('login') ?></label>
                                <div class="form-group">
                                    <input type="text" name="bgcolor" id="put" value="<?= $bgcolor; ?>" />
                                    <input type="color" id="get" value="<?= $bgcolor; ?>" />
                                </div>
                                <div class="form-group">
                                    <a href="<?= admin_url('system_settings/preview_login/'.substr($bgcolor, 1, 6)) ?>" id="btn-preview-login" data-toggle="modal" data-target="#myModal" class="btn btn-success">
                                    <i class="fa fa-external-link"></i><span class="text"> PREVIEW </span>
                                    </a>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border"><?= lang('index_route') ?></legend>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="checkbox" value="1" name="pos"<?= $pos_index ? "checked" : ''; ?>>
                                    <label>
                                        <?= lang("pos", "pos"); ?>
                                    </label>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Local Installation</legend>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="expiry_date">Expiration</label>
                                    <?php echo form_input('expiration', ($this->Settings->expiry_date) ? $this->sma->hrsd($this->Settings->expiry_date) : NULL, 'class="form-control date" id="expiration" placeholder="Expiration" autocomplete="off"'); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="cleafix"></div>
                <div class="form-group">
                    <div class="controls">
                        <?= form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary btn-md"'); ?>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
