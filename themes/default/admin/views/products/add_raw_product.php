<td>
    <?php
        $rp[NULL] = NULL;
        if ($raw_products !== FALSE)
            foreach ($raw_products as $row) $rp[$row->id] = $row->name;

        echo form_dropdown('m_rp_id[]', $rp, NULL, 'class="form-control" data-placeholder="Raw Product"');
    ?>
</td>
<td>
    <input type="text" name="m_rp_qty[]" class="form-control" placeholder="Quantity" />
</td>
<td>
    <button type="button" class="btn btn-default btn-remove-rp"><i class="fa fa-times"></i></button>
</td>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('[name="m_rp_id[]"').select2({minimumResultsForSearch: 7, width: '100%'});
    });
</script>