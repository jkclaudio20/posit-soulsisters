(function ($) {
  "use strict";

	var CURRENT_URL = window.location.href.split('#')[0].split('?')[0];
	var nav = $('[ui-nav]');
	nav.find('li').removeClass('active');
	nav.find('a[href="'+CURRENT_URL+'"]').parent('li').addClass('active');
	nav.find('a[href="'+CURRENT_URL+'"]').parent('li').parent('ul.nav-sub').parent('li').addClass('active');


  
  $(document).on('click', '[ui-nav] a', function (e) {
    var $this = $(e.target), $active, $li;
    $this.is('a') || ($this = $this.closest('a'));
    
    $li = $this.parent();
    $active = $li.siblings( ".active" );
    $li.toggleClass('active');
    $active.removeClass('active');
  });

  jQuery('.malihu-default').mCustomScrollbar({
        theme             : 'dark',
        autoHideScrollbar : true
    });
})(jQuery);
