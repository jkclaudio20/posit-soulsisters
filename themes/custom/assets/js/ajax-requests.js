/**
 * Ajax-requests.js
 *
 * Created: 2018.07.17
 *
 * Use jQuery().data('url') to fetch urls from buttons
 *     form.attr('action') to fetch urls from forms
 */
jQuery(document).ready(function(){
	// Constants
    const _POST            = 'POST';
    const _JSON            = 'JSON';
    const _GET             = 'GET';
    const _PUT             = 'PUT';
    const _MODAL_ERROR_MSG = '<div class="text-center animated fadeIn text-danger"><i class="fa fa-warning"></i> Something went wrong. Please check your internet connection and try again.</div>';
    const _NOTIF_ERROR_MSG = 'Something went wrong. Please check your internet connection and try again.';
    const _BTN_SAVING      = 'Saving';
    const _BTN_DELETING    = 'Deleting';
    const _MODAL_LOADING   = '<h3 class="text-center text-muted"><i class="fa fa-refresh fa-spin"></i></h3>';
    const _WAIT            = 'Please wait'
    const _SPINNER         = '<i class="fa fa-spinner fa-pulse"></i> ';
    const _MODAL_LOADING_SM = '<div class="text-center"><i class="fa fa-refresh fa-spin"></i> Loading...</div>';
    const _NOTIF_SAVING    = {title:'Saving',text:'Please wait...',type:'info'};

    /**
     * Add Notification
     *
     * Uses PNotify to display notifications or messages
     *
     * @param config Object
     * @param clear Boolean. If set to true, all existing Notifications will be cleared before displaying the New Notification
     *              Otherwise, the New Notification will be displayed right away. Default is true.
     * @return PNotify instance
     * @author Karlo
     */
    function add_notification(config, clear = true)
    {
        if (clear !== false) PNotify.removeAll();

        return new PNotify(config);
    }

    jQuery(document).on('shown.bs.modal', '#myModal, #myModal2', function(e){
        var url = jQuery(this).data("remote");

        if (url == undefined || url == '')
        {
            url = e.relatedTarget.href;
        }

        jQuery(this).load(url);
    });  

    // jQuery(document).on('shown.bs.modal', '#myModal, #myModal2', function(e){
    //     alert(jQuery(this).modal('remote'));
    //     return false;

    //     var url = e.relatedTarget.href;
    //     var modal = jQuery(this);

    //     jQuery.ajax({
    //         url      : url,
    //         dataType : 'html',
    //         success  : function(data)
    //         {
    //             modal.html(data);
    //         }
    //     });
    // });

    jQuery(document).on('hidden.bs.modal', '#myModal, #myModal2', function(){
        jQuery(this).html('<div class="modal-dialog "><h3 class="text-center text-white"><i class="fa fa-refresh fa-spin"></i> Loading...</h3></div>');
    });

    // For AJAX Modals
    jQuery(document).on('hidden.bs.modal', '.ajax-modal', function(){
    	var modal = jQuery(this);

    	modal.find('.modal-body').html(_MODAL_LOADING);
    	modal.find('[type=submit]').prop('disabled', true);
    });

    // Show Add New Membership Form
    jQuery(document).on('shown.bs.modal', '#add-membership-modal', function(){
    	var modal  = jQuery(this);
    	var body   = modal.find('.modal-body');
    	var btns   = modal.find('button');
    	var url    = modal.data('url');

    	jQuery.ajax({
    		url      : url,
    		dataType : _JSON,
    		success  : function(data)
    		{
    			body.html(data.html);
    			btns.prop('disabled', false);
    		},
    		error    : function()
    		{
    			body.html(_MODAL_ERROR_MSG);
    		}
    	});
    });

    // Handles submission of New Membership Type form
    jQuery(document).on('submit', '#add-membership-form', function(e){
    	e.preventDefault();

    	var form   = jQuery(this);
    	var btns   = form.find('button');
    	var modal  = form.parents('.modal');

    	if (form.parsley('isValid'))
    	{
    		btns.prop('disabled', true);

    		add_notification({
    			title : 'Saving',
    			text  : 'Please wait...',
    			type  : 'info'
    		});

    		var form_data = {
    			membership_name : form.find('#membership_name').val(),
    			period          : form.find('#period').val(),
    			limited         : form.find('[name="limited"]:checked').val(),
    			no_of_classes   : form.find('#no_of_classes:not(:disabled)').val(),
    			limit_type      : form.find('#limit_type:not(:disabled)').val(),
    			amount          : form.find('#amount').val(),
    			signup_fee      : form.find('#signup_fee').val(),
    			description     : form.find('#description').val()
    		};

    		jQuery.ajax({
    			url      : form.attr('action'),
    			type     : _POST,
    			dataType : _JSON,
    			data     : form_data,
    			success  : function(data)
    			{
    				if (data.result == 'success')
    				{
    					btns.prop('disabled', false);
	    				modal.modal('hide');

	    				jQuery('#types').find('[name="refresh_table"]').val(1).trigger('change');

	    				add_notification({
	    					title : 'Success',
	    					text  : data.message,
	    					type  : 'success'
	    				});
    				}
    				else
    				{
    					btns.prop('disabled', false);

	    				add_notification({
	    					title : 'Error',
	    					text  : data.message,
	    					type  : 'error'
	    				});
    				}
    			},
    			error    : function()
    			{
    				btns.prop('disabled', false);

    				add_notification({
    					title : 'Error',
    					text  : _NOTIF_ERROR_MSG,
    					type  : 'error'
    				});
    			}
    		});
    	}
    });

    // Show Edit Membership Type Form
    jQuery(document).on('shown.bs.modal', '#edit-membership-modal', function(e){
        var modal  = jQuery(this);
        var body   = modal.find('.modal-body');
        var btns   = modal.find('button');
        var url    = modal.data('url');
        var id     = e.relatedTarget.dataset.id;

        jQuery.ajax({
            url      : url,
            type     : _POST,
            dataType : _JSON,
            data     : {id : id},
            success  : function(data)
            {
                body.html(data.html);
                if (data.result == 'success') btns.prop('disabled', false);
            },
            error    : function()
            {
                body.html(_MODAL_ERROR_MSG);
            }
        });
    });

    // Show Membership Type Details
    jQuery(document).on('shown.bs.modal', '#view-membership-modal', function(e){
        var modal  = jQuery(this);
        var body   = modal.find('.modal-body');
        var btns   = modal.find('button');
        var url    = modal.data('url');
        var id     = e.relatedTarget.dataset.id;

        jQuery.ajax({
            url      : url,
            type     : _POST,
            dataType : _JSON,
            data     : {id : id},
            success  : function(data)
            {
                body.html(data.html);
                btns.prop('disabled', false);
            },
            error    : function()
            {
                body.html(_MODAL_ERROR_MSG);
            }
        });
    });

    // Handles submission of Edit Membership Type form
    jQuery(document).on('submit', '#edit-membership-form', function(e){
    	e.preventDefault();

    	var form   = jQuery(this);
    	var btns   = form.find('button');
    	var modal  = form.parents('.modal');

    	if (form.parsley('isValid'))
    	{
    		btns.prop('disabled', true);

    		add_notification({
    			title : 'Updating',
    			text  : 'Please wait...',
    			type  : 'info'
    		});

    		var form_data = {
                id              : form.find('[name="membership_type_id"]').val(),
    			membership_name : form.find('#membership_name').val(),
    			period          : form.find('#period').val(),
    			limited         : form.find('[name="limited"]:checked').val(),
    			no_of_classes   : form.find('#no_of_classes:not(:disabled)').val(),
    			limit_type      : form.find('#limit_type:not(:disabled)').val(),
    			amount          : form.find('#amount').val(),
    			signup_fee      : form.find('#signup_fee').val(),
    			description     : form.find('#description').val()
    		};

    		jQuery.ajax({
    			url      : form.attr('action'),
    			type     : _POST,
    			dataType : _JSON,
    			data     : form_data,
    			success  : function(data)
    			{
    				if (data.result == 'success')
    				{
    					btns.prop('disabled', false);
	    				modal.modal('hide');

	    				jQuery('#types').find('[name="refresh_table"]').val(1).trigger('change');

	    				add_notification({
	    					title : 'Success',
	    					text  : data.message,
	    					type  : 'success'
	    				});
    				}
    				else
    				{
    					btns.prop('disabled', false);

	    				add_notification({
	    					title : 'Error',
	    					text  : data.message,
	    					type  : 'error'
	    				});
    				}
    			},
    			error    : function()
    			{
    				btns.prop('disabled', false);

    				add_notification({
    					title : 'Error',
    					text  : _NOTIF_ERROR_MSG,
    					type  : 'error'
    				});
    			}
    		});
    	}
    });

    // Show Delete Confirmation Box for Membership Type
    jQuery(document).on('show.bs.modal', '#delete-membership-modal', function(e){
        var modal  = jQuery(this);
        var body   = modal.find('.modal-body');
        var btn    = modal.find('.confirm-delete');
        var id     = e.relatedTarget.dataset.id;
        var name   = e.relatedTarget.dataset.name;

        body.find('#name').html(name);
        btn.data({id : id, name : name});
    });

    // Membership Type Deletion confirmed
    jQuery(document).on('click', '#delete-membership-modal .confirm-delete', function(){
        var btn   = jQuery(this);
        var modal = btn.parents('.modal');
        var btns  = modal.find('.modal-footer').find('button');
        var id    = btn.data('id');
        var name  = btn.data('name');
        var url   = btn.data('url');

        btns.prop('disabled', true);

        add_notification({
            title : 'Deleting',
            text  : 'Please wait...',
            type  : 'info'
        });

        jQuery.ajax({
            url      : url,
            type     : _POST,
            dataType : _JSON,
            data     : {id : id, name : name},
            success  : function(data)
            {
                if (data.result == 'success')
                {
                    btns.prop('disabled', false);
                    modal.modal('hide');

                    jQuery('#types').find('[name="refresh_table"]').val(1).trigger('change');

                    add_notification({
                        title : 'Success',
                        text  : data.message,
                        type  : 'success'
                    });
                }
                else
                {
                    btns.prop('disabled', false);

                    add_notification({
                        title : 'Error',
                        text  : data.message,
                        type  : 'error'
                    });
                }
            },
            error    : function()
            {
                btns.prop('disabled', false);

                add_notification({
                    title : 'Error',
                    text  : _NOTIF_ERROR_MSG,
                    type  : 'error'
                });
            }
        });
    });

    // Handles submission of Add New Member Form
    jQuery(document).on('submit', '#add-member-form', function(e){
        e.preventDefault();

        var form = jQuery(this);
        var btns = form.find('.box-footer').find('button');

        if (form.parsley('isValid'))
        {
            btns.prop('disabled', true);

            add_notification(_NOTIF_SAVING);

            var form_data = new FormData(this);

            jQuery.ajax({
                url         : form.attr('action'),
                type        : _POST,
                dataType    : _JSON,
                data        : form_data,
                contentType : false,
                processData : false,
                success     : function(data)
                {
                    if (data.result == 'success')
                    {
                        add_notification({
                            title : 'Redirecting',
                            text  : 'Please wait...',
                            type  : 'info'
                        });

                        window.location.href = data.url;
                    }
                    else
                    {
                        btns.prop('disabled', false);

                        add_notification({
                            title : 'Error',
                            text  : data.message,
                            type  : 'error'
                        });
                    }
                },
                error        : function()
                {
                    btns.prop('disabled', false);

                    add_notification({
                        title : 'Error',
                        text  : _NOTIF_ERROR_MSG,
                        type  : 'error'
                    });
                }
            });
        }
    });

    // Show Delete Confirmation Box for Member
    jQuery(document).on('show.bs.modal', '#delete-member-modal', function(e){
        var modal  = jQuery(this);
        var body   = modal.find('.modal-body');
        var btn    = modal.find('.confirm-delete');
        var id     = e.relatedTarget.dataset.id;
        var name   = e.relatedTarget.dataset.name;

        body.find('#name').html(name);
        btn.data({id : id, name : name});
    });

    // Member Deletion confirmed
    jQuery(document).on('click', '#delete-member-modal .confirm-delete', function(){
        var btn   = jQuery(this);
        var modal = btn.parents('.modal');
        var btns  = modal.find('.modal-footer').find('button');
        var id    = btn.data('id');
        var name  = btn.data('name');
        var url   = btn.data('url');

        btns.prop('disabled', true);

        add_notification({
            title : 'Deleting',
            text  : 'Please wait...',
            type  : 'info'
        });

        jQuery.ajax({
            url      : url,
            type     : _POST,
            dataType : _JSON,
            data     : {id : id, name : name},
            success  : function(data)
            {
                if (data.result == 'success')
                {
                    btns.prop('disabled', false);
                    modal.modal('hide');

                    if (btn.data('goto') != '' && btn.data('goto') != undefined)
                    {
                        window.location.href = btn.data('goto');
                    }
                    else
                    {
                        jQuery('.members-box').find('[name="refresh_table"]').val(1).trigger('change');

                        add_notification({
                            title : 'Success',
                            text  : data.message,
                            type  : 'success'
                        });
                    }
                }
                else
                {
                    btns.prop('disabled', false);

                    add_notification({
                        title : 'Error',
                        text  : data.message,
                        type  : 'error'
                    });
                }
            },
            error    : function()
            {
                btns.prop('disabled', false);

                add_notification({
                    title : 'Error',
                    text  : _NOTIF_ERROR_MSG,
                    type  : 'error'
                });
            }
        });
    });
});
