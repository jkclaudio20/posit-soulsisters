<div class="padding">
    <div class="row m-b">
        <div class="col-xs-12 col-lg-12">
            <div class="box animated fadeIn">
                <div class="box-header dker">
                    <h3><i class="material-icons">people</i> Members</h3>

                    <div class="box-tool">
                        <ul class="nav">
                            <li class="nav-item inline dropdown">
                                <a href="#" data-toggle="dropdown" class="nav-link">
                                    <i class="material-icons tip" data-title="Options">more_vert</i>
                                </a>

                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="<?= admin_url('members/add'); ?>">
                                            <i class="material-icons">person_add</i> New Member
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <small>Below is a list of all members</small>
                </div>
                <div class="box-body members-box">
                    <?= form_hidden('refresh_table', 0); ?>
                    <div class="table-responsive">
                        <table class="table table-hover table-striped" id="members-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Joining Date</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="delete-member-modal" class="modal fade" data-backdrop="true">
	<div class="row-col h-v">
     	<div class="row-cell v-m">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title"><i class="fa fa-question-circle"></i> Delete Confirmation</h5>
					</div>
					<div class="modal-body text-center">
						<p>
							Are you sure to delete <span id="name" class="_600"></span> from Members?
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn danger p-x-md confirm-delete" data-url="<?= admin_url('membership/ajax_delete_member') ?>">Delete</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        var table = jQuery('#members-table').DataTable({
            bProcessing : true,
			sAjaxSource : '<?= admin_url('membership/members_json_data'); ?>',
			aoColumns   : [
				{ mData : 'counter', sWidth : '5%', sClass : 'text-center' },
				{ mData : 'name' },
				{ mData : 'join_date' },
                { mData : 'status', sClass : 'text-center' },
				{ mData : 'actions', sClass : 'text-center' }
			]
        });
    });
</script>
