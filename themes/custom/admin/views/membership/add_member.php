<div class="padding">
    <div class="row m-b">
        <div class="col-xs-12 col-lg-12">
            <div class="box animated fadeIn">
                <?= admin_form_open('membership/ajax_add_member', 'data-parsley-validate id="add-member-form"'); ?>
                <div class="box-header dker">
                    <h3><i class="material-icons">person_add</i> New Member</h3>

                    <div class="box-tool">
                        <ul class="nav">
                            <li class="nav-item dropdown inline">
                                <a class="nav-link" data-toggle="dropdown">
                                    <i class="material-icons">more_vert</i>
                                </a>

                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a href="<?= admin_url('members'); ?>">
                                            <i class="material-icons">people</i> Members List
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <small><?= lang('enter_info'); ?></small>
                </div>

                <div class="box-body">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Personal Information</legend>
                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Name *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <?= form_input($first_name, '', 'class="form-control" placeholder="First Name *" required="required"'); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <?= form_input($middle_name, '', 'class="form-control" placeholder="Middle Name"'); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <?= form_input($last_name, '', 'class="form-control" placeholder="Last Name *" required="required"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Gender *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <div class="row form-control-static">
                                    <label class="checkbox col-xs-12 col-md-2">
                                        <?= form_radio('gender', 'male', FALSE, 'class="gender" required="required"'); ?> Male
                                    </label>
                                    <label class="checkbox col-xs-12 col-md-2">
                                        <?= form_radio('gender', 'female', FALSE, 'class="gender" required="required"'); ?> Female
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Date of Birth *</label>
                            <div class="col-xs-12 col-sm-5 col-md-5">
                                <?= form_input($birthdate, '', 'class="form-control datepicker-birthdate" placeholder="Date of Birth" autocomplete="off" required="required"'); ?>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Contact Information</legend>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Address *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 form-group">
                                        <?= form_input($street, '', 'class="form-control" placeholder="Street"'); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-43 form-group">
                                        <?= form_input($barangay, '', 'class="form-control" placeholder="Barangay"'); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 form-group">
                                        <?= form_input($city, '', 'class="form-control" placeholder="City / Municipality *" required="required"'); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 form-group">
                                        <?= form_input($province, '', 'class="form-control" placeholder="Province *" required="required"'); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 form-group">
                                        <?= form_input($zip, '', 'class="form-control" placeholder="ZIP Code"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Phone *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <div class="input-group">
                                    <span class="input-group-addon">+63</span>
                                    <?= form_input($phone, '', 'class="form-control" placeholder="Phone" required="required"'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Email *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <?= form_input($email, '', 'class="form-control" data-parsley-type="email" placeholder="Email" required="required"'); ?>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Physical Information</legend>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Weight</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <div class="input-group">
                                            <?= form_input($weight, '', 'class="form-control" placeholder="Weight"'); ?>
                                            <span class="input-group-addon">kg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Height</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <div class="input-group">
                                            <?= form_input($height, '', 'class="form-control" placeholder="Height"'); ?>
                                            <span class="input-group-addon">cm</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Chest</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <div class="input-group">
                                            <?= form_input($chest, '', 'class="form-control" placeholder="Chest"'); ?>
                                            <span class="input-group-addon">inches</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Waist</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <div class="input-group">
                                            <?= form_input($waist, '', 'class="form-control" placeholder="Waist"'); ?>
                                            <span class="input-group-addon">inches</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Thigh</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <div class="input-group">
                                            <?= form_input($thigh, '', 'class="form-control" placeholder="Thigh"'); ?>
                                            <span class="input-group-addon">inches</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Arms</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <div class="input-group">
                                            <?= form_input($arms, '', 'class="form-control" placeholder="Arms"'); ?>
                                            <span class="input-group-addon">inches</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Fat</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <div class="input-group">
                                            <?= form_input($fat, '', 'class="form-control" placeholder="Fat"'); ?>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Membership Information</legend>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Join Date *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <?= form_input($join_date, '', 'class="form-control datepicker-default" placeholder="Join Date" required="required"'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Membership *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <?= form_dropdown('membership_type_id', $membership_types, NULL, 'class="form-control select" id="membership_type_id" data-placeholder="Membership Type" required="required"'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Membership Validity *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <div class="input-group">
                                    <?= form_input($from, '', 'class="form-control datepicker-default" placeholder="Start" required="required"'); ?>
                                    <span class="input-group-addon">to</span>
                                    <?= form_input($to, '', 'class="form-control datepicker-default" placeholder="End" required="required"'); ?>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Login Information</legend>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Username *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <?= form_input($username, '', 'class="form-control" placeholder="Username" required="required"'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Password *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <?= form_input($password, '', 'class="form-control" data-parsley-equalto="#password_confirm" placeholder="Password" required="required"'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Confirm Password *</label>
                            <div class="col-xs-12 col-sm-10 col-md-10">
                                <?= form_input($password_confirm, '', 'class="form-control" data-parsley-equalto="#password" placeholder="Confirm Password" required="required"'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="box-footer text-right dker">
                    <button type="reset" class="md-btn md-raised m-b-sm w-sm dark-white">Reset</button>
                    <button type="submit" class="md-btn md-raised m-b-sm w-sm primary">Add Member</button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
