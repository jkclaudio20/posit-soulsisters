<?php if ($data !== FALSE): ?>
	<form>
		<div class="form-group row">
			<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Membership Name:</label>
			<div class="col-xs-12 col-sm-9 col-md-9">
				<p class="form-control-static _600">
					<?= htmlspecialchars($data->membership_name); ?>
				</p>
			</div>
		</div>

		<div class="form-group row">
			<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Membership Period:</label>
			<div class="col-xs-12 col-sm-9 col-md-9">
				<p class="form-control-static _600">
					<?= $data->period ?> days
				</p>
			</div>
		</div>

		<div class="form-group row">
			<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Limit:</label>
			<div class="col-xs-12 col-sm-9 col-md-9">
				<p class="form-control-static _600">
					<?php
						if ($data->limited == 1)
						{
							echo ($data->no_of_classes > 1) ? $data->no_of_classes . ' classes' : $data->no_of_classes . ' class';
							echo ($data->limit_type == 'monthly') ? ' every month' : ' every week';
						}
						else
						{
							echo 'Unlimited';
						}
					?>
				</p>
			</div>
		</div>

		<div class="form-group row">
			<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Amount:</label>
			<div class="col-xs-12 col-sm-9 col-md-9">
				<p class="form-control-static _600">
					<?= number_format($data->amount, 2) ?>
				</p>
			</div>
		</div>

		<div class="form-group row">
			<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Signup Fee:</label>
			<div class="col-xs-12 col-sm-9 col-md-9">
				<p class="form-control-static _600">
					<?= number_format($data->signup_fee, 2) ?>
				</p>
			</div>
		</div>

		<div class="form-group row">
			<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Description:</label>
			<div class="col-xs-12 col-sm-9 col-md-9">
				<p class="form-control-static _600">
					<?= ($data->description) ? htmlspecialchars($data->description) : '<span class="text-muted">No description available</span>' ?>
				</p>
			</div>
		</div>

		<div class="form-group row">
			<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Date Created:</label>
			<div class="col-xs-12 col-sm-9 col-md-9">
				<p class="form-control-static _600">
					<?php
						$date_created = new DateTime($data->date_created);

						echo $date_created->format('F j, Y h:i A');
					?>
				</p>
			</div>
		</div>

		<div class="form-group row">
			<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Created by:</label>
			<div class="col-xs-12 col-sm-9 col-md-9">
				<p class="form-control-static _600">
					<?= ( ! is_null($data->id)) ? $data->first_name . ' ' . $data->last_name : '<span class="text-muted">Unknown</span>' ?>
				</p>
			</div>
		</div>
	</form>
<?php else: ?>
	<h5 class="text-center"><i class="fa fa-warning"></i> Membership Type not Found</h5>
	<div class="text-center">This membership type no longer exists.</div>
<?php endif; ?>