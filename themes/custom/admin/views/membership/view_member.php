<div class="padding">
    <div class="row m-b">
        <div class="col-xs-12 col-lg-12">
            <div class="box animated fadeIn">
                <div class="box-header dker">
                    <h3><i class="material-icons">person</i> Member Information</h3>

                    <?php if ($data !== FALSE): ?>

                    <div class="box-tool">
    					<ul class="nav">
    						<li data-toggle="dropdown" class="nav-item inline dropdown">
    							<a class="nav-link" href="#"><i class="material-icons tip" data-title="Options">more_vert</i></a>
    						</li>
    						<ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dLabel">
                                <li>
    								<a href="<?= admin_url('members/edit?id='.$member_id) ?>">
    									<i class="material-icons">edit</i> Edit
    								</a>
    							</li>
                                <li>
    								<a data-toggle="modal" data-target="#delete-member-modal" data-id="<?= $member_id ?>" data-name="<?= $data->first_name . ' ' . $data->last_name ?>">
    									<i class="material-icons">delete</i> Delete
    								</a>
    							</li>
                                <div class="dropdown-divider"></div>
                                <li>
                                    <a href="<?= admin_url('members'); ?>">
                                        <i class="material-icons">people</i> Members List
                                    </a>
                                </li>
                                <li>
    								<a href="<?= admin_url('members/add') ?>">
    									<i class="material-icons">person_add</i> New Member
    								</a>
    							</li>
    						</ul>
    					</ul>
    				</div>

                    <small>
                        <?php
                            $middle_name = ($data->middle_name) ? ' '.$data->middle_name : NULL;

                            echo $data->first_name . $middle_name . ' '.$data->last_name.'\'s';
                        ?>
                        Personal, Contact, Physical, and Membership Information
                    </small>

                    <?php endif; ?>

                </div>
                <div class="box-body">
                    <?php if ($data !== FALSE): ?>
                        <form>
                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Personal Information</legend>

                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Name:</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <p class="form-control-static _600">
                                            <?php
                                                $middle_name = ($data->middle_name) ? ' '.$data->middle_name : NULL;

                                                echo htmlspecialchars($data->first_name.$middle_name.' '.$data->last_name);
                                            ?>
                                        </p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Gender:</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <p class="form-control-static _600">
                                            <?= '<i class="fa fa-'.$data->gender.'"></i> '.ucfirst($data->gender); ?>
                                        </p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Date of Birth:</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <p class="form-control-static _600">
                                            <?php
                                                $birthdate = new DateTime($data->birthdate);

                                                echo $birthdate->format('F j, Y');
                                            ?>
                                        </p>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Contact Information</legend>

                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Address:</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <p class="form-control-static _600">
                                            <?php
                                                echo ($data->street)   ? $data->street.', '   : NULL;
                                                echo ($data->barangay) ? $data->barangay.', ' : NULL;
                                                echo ($data->city)     ? $data->city.', '     : NULL;
                                                echo $data->province;
                                                echo ($data->zip) ? ' '.$data->zip : NULL;
                                            ?>
                                        </p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Phone:</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <p class="form-control-static _600">
                                            <span class="text-muted">+63 </span><?= $data->phone ?>
                                        </p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Email:</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <p class="form-control-static _600">
                                            <?= htmlspecialchars($data->email); ?>
                                        </p>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Physical Information</legend>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group row">
                                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Weight:</label>
                                            <div class="col-xs-12 col-sm-10 col-md-10">
                                                <p class="form-control-static _600">
                                                    <?= ($data->weight) ? $data->weight . ' kg' : '<span class="text-muted">Not Set</span>'; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group row">
                                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Height:</label>
                                            <div class="col-xs-12 col-sm-10 col-md-10">
                                                <p class="form-control-static _600">
                                                    <?= ($data->height) ? $data->height . ' cm' : '<span class="text-muted">Not Set</span>'; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group row">
                                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Chest:</label>
                                            <div class="col-xs-12 col-sm-10 col-md-10">
                                                <p class="form-control-static _600">
                                                    <?= ($data->chest) ? $data->chest . ' inches' : '<span class="text-muted">Not Set</span>'; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group row">
                                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Waist:</label>
                                            <div class="col-xs-12 col-sm-10 col-md-10">
                                                <p class="form-control-static _600">
                                                    <?= ($data->waist) ? $data->waist . ' inches' : '<span class="text-muted">Not Set</span>'; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group row">
                                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Thigh:</label>
                                            <div class="col-xs-12 col-sm-10 col-md-10">
                                                <p class="form-control-static _600">
                                                    <?= ($data->thigh) ? $data->thigh . ' inches' : '<span class="text-muted">Not Set</span>'; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group row">
                                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Arms:</label>
                                            <div class="col-xs-12 col-sm-10 col-md-10">
                                                <p class="form-control-static _600">
                                                    <?= ($data->arms) ? $data->arms . ' inches' : '<span class="text-muted">Not Set</span>'; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                        <div class="form-group row">
                                            <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Fat:</label>
                                            <div class="col-xs-12 col-sm-10 col-md-10">
                                                <p class="form-control-static _600">
                                                    <?= ($data->fat) ? $data->fat . '%' : '<span class="text-muted">Not Set</span>'; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="scheduler-border">
                                <legend class="scheduler-border">Membership Information</legend>

                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Join Date:</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <p class="form-control-static _600">
                                            <?php
                                                $join_date = new DateTime($data->join_date);

                                                echo $join_date->format('F j, Y');
                                            ?>
                                        </p>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-2 col-md-2">Membership History:</label>
                                    <div class="col-xs-12 col-sm-10 col-md-10">
                                        <table class="table table-hover table-striped" id="membership-history-table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Validity</th>
                                                    <th>Membership Type</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    <?php else: ?>
                        <h5>
                            <i class="fa fa-warning fa-fw"></i>
                            Member not found
                        </h5>
                        <a href="<?= admin_url('members'); ?>" class="md-btn md-raised m-b-sm success">Go Back to Members List</a>
                    <?php endif; ?>
                </div>
                <div class="box-footer dker text-right">
                    <button data-href="<?= admin_url('members/edit/'.$member_id); ?>" class="md-btn md-raised w-sm m-b-sm success redirect">Edit</button>
                    <button type="button" class="md-btn md-raised m-b-sm w-sm danger" data-toggle="modal" data-target="#delete-member-modal" data-id="<?= $member_id; ?>" data-name="<?= $data->first_name . ' ' . $data->last_name ?>">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="delete-member-modal" class="modal fade" data-backdrop="true">
	<div class="row-col h-v">
     	<div class="row-cell v-m">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title"><i class="fa fa-question-circle"></i> Delete Confirmation</h5>
					</div>
					<div class="modal-body text-center">
						<p>
							Are you sure to delete <span id="name" class="_600"></span> from Members?
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn danger p-x-md confirm-delete" data-url="<?= admin_url('membership/ajax_delete_member') ?>" data-goto="<?= admin_url('members'); ?>">Delete</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function(){
        var table = jQuery('#membership-history-table').DataTable({
            bProcessing : true,
            bFilter     : false,
            bInfo       : false,
            bPaginate   : false,
			sAjaxSource : '<?= admin_url('membership/membership_history_json_data?id='.$member_id); ?>',
			aoColumns   : [
				{ mData : 'counter', sWidth : '5%', sClass : 'text-center' },
				{ mData : 'validity' },
				{ mData : 'membership_type' }
			]
        });
    });
</script>
