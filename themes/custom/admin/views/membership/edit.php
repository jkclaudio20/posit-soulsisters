<?php if ($data !== FALSE): ?>
	<?php echo form_hidden('membership_type_id', $data->membership_type_id); ?>

	<div class="form-group row">
		<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Membership Name *</label>
		<div class="col-xs-12 col-sm-9 col-md-9">
			<?php echo form_input($membership_name, $data->membership_name, 'class="form-control" placeholder="Membership Name" required="required"'); ?>
		</div>
	</div>

	<div class="form-group row">
		<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Period *</label>
		<div class="col-xs-12 col-sm-9 col-md-9">
			<div class="input-group">
				<span class="input-group-addon">No. of Days</span>
				<?php echo form_input($period, $data->period, 'class="form-control" placeholder="Membership Period" data-parsley-type="number" required="required"'); ?>
			</div>
		</div>
	</div>

	<?php
		$limited   = ($data->limited == 1) ? ' checked="checked"' : NULL;
		$unlimited = ($data->limited != 1) ? ' checked="checked"' : NULL;
	?>

	<div class="form-group row">
		<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Limit *</label>
		<div class="col-xs-12 col-sm-9 col-md-9 form-control-static">
			<div class="row">
				<div class="col-xs-12 col-md-3">
					<label class="checkbox">
						<?php echo form_radio('limited', 1, FALSE, 'required="required"'.$limited); ?> Limited
					</label>
				</div>
				<div class="col-xs-12 col-md-3">
					<label class="checkbox">
						<?php echo form_radio('limited', 0, FALSE, 'required="required"'.$unlimited); ?> Unlimited
					</label>
				</div>
			</div>
		</div>
	</div>

	<div id="limit-options" style="display: none;">
		<div class="form-group row">
			<div class="col-xs-12 col-sm-9 offset-sm-3 col-md-9 offset-md-3">
				<div class="input-group">
					<?php echo form_input($no_of_classes, $data->no_of_classes, 'class="form-control" placeholder="No. of Classes" disabled="disabled"'); ?>
					<span class="input-group-addon">classes</span>
					<?php echo form_dropdown('limit_type', $limit_types, $data->limit_type, 'class="form-control" id="limit_type" disabled="disabled"'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group row">
		<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Amount *</label>
		<div class="col-xs-12 col-sm-9 col-md-9">
			<?php echo form_input($amount, $data->amount, 'class="form-control" placeholder="Membership Amount" required="required"'); ?>
		</div>
	</div>

	<div class="form-group row">
		<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Signup Fee *</label>
		<div class="col-xs-12 col-sm-9 col-md-9">
			<?php echo form_input($signup_fee, $data->signup_fee, 'class="form-control" placeholder="Signup Fee" required="required"'); ?>
		</div>
	</div>

	<div class="form-group row">
		<label class="form-control-label col-xs-12 col-sm-3 col-md-3">Description</label>
		<div class="col-xs-12 col-sm-9 col-md-9">
			<?php echo form_textarea($description, $data->description, 'class="form-control" placeholder="Type description here... (Optional)"'); ?>
		</div>
	</div>

	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('.checkbox').iCheck({
				checkboxClass:'icheckbox_flat-green',
				radioClass:'iradio_flat-green',
				increaseArea:'20%'
			});

			set_limit_status();

			function set_limit_status()
			{
				var container = jQuery('#limit-options');

				if (jQuery('[name="limited"]:checked').val() == 1)
				{
					container.find('input, select').prop({disabled:false,required:true});
					container.slideDown();
					jQuery('#no_of_classes').trigger('input');
				}
				else
				{
					container.slideUp();
					container.find('input, select').prop({disabled:true,required:false});
				}
			}
		});
	</script>
<?php else: ?>
	<h5 class="text-center"><i class="fa fa-warning"></i> Membership Type not Found</h5>
	<div class="text-center">This membership type no longer exists.</div>
<?php endif; ?>
