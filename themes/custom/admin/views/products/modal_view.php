<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel"><?= $product->name.(SHOP && $product->hide != 1 ? ' ('.lang('shop_views').': '.$product->views.')' : ''); ?></h4>
            <button type="button" class="btn btn-sm white no-print pull-right" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-xs-5">
                    <img id="pr-image" src="<?= base_url() ?>assets/uploads/<?= $product->image ?>"
                    alt="<?= $product->name ?>" class="img-responsive img-thumbnail"/>

                    <div id="multiimages" class="padding10">
                        <?php if (!empty($images)) {
                            echo '<a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $product->image . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $product->image . '" alt="' . $product->image . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                            foreach ($images as $ph) {
                                echo '<div class="gallery-image"><a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $ph->photo . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $ph->photo . '" alt="' . $ph->photo . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                if ($Owner || $Admin || $GP['products-edit']) {
                                    echo '<a href="#" class="delimg" data-item-id="'.$ph->id.'"><i class="fa fa-times"></i></a>';
                                }
                                echo '</div>';
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col-xs-7">
                    <form>
                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("barcode_qrcode"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <img src="<?= admin_url('misc/barcode/'.$product->code.'/'.$product->barcode_symbology.'/74/0'); ?>" alt="<?= $product->code; ?>" class="bcimg" />
                                    <?= $this->sma->qrcode('link', urlencode(admin_url('products/view/' . $product->id)), 2); ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("type"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?= lang($product->type); ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("name"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?= $product->name; ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("code"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?= $product->code; ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("brand"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?= ($brand) ? $brand->name : '<span class="text-muted">No brand</span>'; ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("category"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?= $category->name; ?>
                                </p>
                            </div>
                        </div>

                        <?php if ($product->subcategory_id): ?>
                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("subcategory"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?= $subcategory->name; ?>
                                </p>
                            </div>
                        </div>
                        <?php endif; ?>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("unit"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?= $unit ? $unit->name.' ('.$unit->code.')' : ''; ?>
                                </p>
                            </div>
                        </div>

                        <?php if ($Owner || $Admin): ?>
                            <div class="form-group row">
                                <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("cost"); ?></label>
                                <div class="col-xs-12 col-sm-9 col-md-9">
                                    <p class="form-control-static _600">
                                        <?= $this->sma->formatMoney($product->cost); ?>
                                    </p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("price"); ?></label>
                                <div class="col-xs-12 col-sm-9 col-md-9">
                                    <p class="form-control-static _600">
                                        <?= $this->sma->formatMoney($product->price); ?>
                                    </p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("custom_price"); ?></label>
                                <div class="col-xs-12 col-sm-9 col-md-9">
                                    <p class="form-control-static _600">
                                        <?= $this->sma->formatMoney($product->custom_price); ?>
                                    </p>
                                </div>
                            </div>
                        <?php else: ?>
                            <?php if ($this->session->userdata('show_cost')): ?>
                            <div class="form-group row">
                                <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("cost"); ?></label>
                                <div class="col-xs-12 col-sm-9 col-md-9">
                                    <p class="form-control-static _600">
                                        <?= $this->sma->formatMoney($product->cost); ?>
                                    </p>
                                </div>
                            </div>
                            <?php endif; ?>

                            <?php if ($this->session->userdata('show_price')): ?>
                            <div class="form-group row">
                                <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("price"); ?></label>
                                <div class="col-xs-12 col-sm-9 col-md-9">
                                    <p class="form-control-static _600">
                                        <?= $this->sma->formatMoney($product->price); ?>
                                    </p>
                                </div>
                            </div>
                                <?php if ($product->promotion): ?>
                                <div class="form-group row">
                                    <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("promotion"); ?></label>
                                    <div class="col-xs-12 col-sm-9 col-md-9">
                                        <p class="form-control-static _600">
                                            <?= $this->sma->formatMoney($product->promo_price) . ' ('.$this->sma->hrsd($product->start_date).' - '.$this->sma->hrsd($product->start_date).')'; ?>
                                        </p>
                                    </div>
                                </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if ($product->tax_rate): ?>
                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("tax_rate"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?= $tax_rate->name; ?>
                                </p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("tax_method"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?= $product->tax_method == 0 ? lang('inclusive') : lang('exclusive'); ?>
                                </p>
                            </div>
                        </div>
                        <?php endif; ?>

                        <?php if ($product->alert_quantity != 0): ?>
                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("alert_quantity"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?= $this->sma->formatQuantity($product->alert_quantity); ?>
                                </p>
                            </div>
                        </div>
                        <?php endif; ?>

                        <?php if ($variants): ?>
                        <div class="form-group row">
                            <label class="form-control-label col-xs-12 col-sm-3 col-md-3"><?= lang("product_variants"); ?></label>
                            <div class="col-xs-12 col-sm-9 col-md-9">
                                <p class="form-control-static _600">
                                    <?php foreach ($variants as $variant) {
                                        echo '<span class="label label-primary">' . $variant->name . '</span> ';
                                    } ?>
                                </p>
                            </div>
                        </div>
                        <?php endif; ?>
                    </form>
                </div>

                <div class="col-xs-12">
                        <div class="row">
                            <div class="col-xs-5">
                                <?php if ($product->cf1 || $product->cf2 || $product->cf3 || $product->cf4 || $product->cf5 || $product->cf6) { ?>
                                <h3 class="bold"><?= lang('custom_fields') ?></h3>
                                <div class="table-responsive">
                                    <table
                                    class="table table-bordered table-striped table-condensed dfTable two-columns">
                                    <thead>
                                        <tr>
                                            <th><?= lang('custom_field') ?></th>
                                            <th><?= lang('value') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if ($product->cf1) {
                                            echo '<tr><td>' . lang("pcf1") . '</td><td>' . $product->cf1 . '</td></tr>';
                                        }
                                        if ($product->cf2) {
                                            echo '<tr><td>' . lang("pcf2") . '</td><td>' . $product->cf2 . '</td></tr>';
                                        }
                                        if ($product->cf3) {
                                            echo '<tr><td>' . lang("pcf3") . '</td><td>' . $product->cf3 . '</td></tr>';
                                        }
                                        if ($product->cf4) {
                                            echo '<tr><td>' . lang("pcf4") . '</td><td>' . $product->cf4 . '</td></tr>';
                                        }
                                        if ($product->cf5) {
                                            echo '<tr><td>' . lang("pcf5") . '</td><td>' . $product->cf5 . '</td></tr>';
                                        }
                                        if ($product->cf6) {
                                            echo '<tr><td>' . lang("pcf6") . '</td><td>' . $product->cf6 . '</td></tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php } ?>

                    <div class="col-xs-7">
                        <?php if ((!$Supplier || !$Customer) && !empty($warehouses) && $product->type == 'standard') { ?>
                            <h3 class="bold"><?= lang('warehouse_quantity') ?></h3>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-condensed dfTable two-columns">
                                    <thead>
                                        <tr>
                                            <th><?= lang('warehouse_name') ?></th>
                                            <th><?= lang('quantity') . ' (' . lang('rack') . ')'; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($warehouses as $warehouse) {
                                            if ($warehouse->quantity != 0) {
                                                echo '<tr><td>' . $warehouse->name . ' (' . $warehouse->code . ')</td><td><strong>' . $this->sma->formatQuantity($warehouse->quantity) . '</strong>' . ($warehouse->rack ? ' (' . $warehouse->rack . ')' : '') . '</td></tr>';
                                            }
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    
                        <?php if ($product->type == 'combo') { ?>
                        <h3 class="bold"><?= lang('combo_items') ?></h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed dfTable two-columns">
                                <thead>
                                    <tr>
                                        <th><?= lang('product_name') ?></th>
                                        <th><?= lang('quantity') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($combo_items as $combo_item) {
                                        echo '<tr><td>' . $combo_item->name . ' (' . $combo_item->code . ') </td><td>' . $this->sma->formatQuantity($combo_item->qty) . '</td></tr>';
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } ?>
                    

                        <?php if (!empty($options)) { ?>
                        <h3 class="bold"><?= lang('product_variants_quantity'); ?></h3>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed dfTable">
                                <thead>
                                    <tr>
                                        <th><?= lang('warehouse_name') ?></th>
                                        <th><?= lang('product_variant'); ?></th>
                                        <th><?= lang('quantity') . ' (' . lang('rack') . ')'; ?></th>
                                        <?php if ($Owner || $Admin) {
                                            echo '<th>' . lang('price_addition') . '</th>';
                                        } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($options as $option) {
                                        if ($option->wh_qty != 0) {
                                            echo '<tr><td>' . $option->wh_name . '</td><td>' . $option->name . '</td><td class="text-center">' . $this->sma->formatQuantity($option->wh_qty) . '</td>';
                                            if ($Owner || $Admin && (!$Customer || $this->session->userdata('show_cost'))) {
                                                echo '<td class="text-right">' . $this->sma->formatMoney($option->price) . '</td>';
                                            }
                                            echo '</tr>';
                                        }

                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } ?>
                    </div>

                    <div class="col-xs-12">
                        <?= $product->details ? '<div class="panel panel-success"><div class="panel-heading">' . lang('product_details_for_invoice') . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; ?>
                        <?= $product->product_details ? '<div class="panel panel-primary"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; ?>
                    </div>

                    <div class="col-xs-12">
                        <?php if (!$Supplier || !$Customer) { ?>
                            <div class="buttons">
                                <div class="btn-group btn-group-justified">
                                    <div class="btn-group">
                                        <a href="<?= admin_url('products/print_barcodes/' . $product->id) ?>" class="tip btn btn-primary" title="<?= lang('print_barcode_label') ?>">
                                            <i class="fa fa-print"></i>
                                            <span class="hidden-sm hidden-xs"><?= lang('print_barcode_label') ?></span>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="<?= admin_url('products/pdf/' . $product->id) ?>" class="tip btn btn-primary" title="<?= lang('pdf') ?>">
                                            <i class="fa fa-download"></i>
                                            <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="<?= admin_url('products/edit/' . $product->id) ?>" class="tip btn btn-warning tip" title="<?= lang('edit_product') ?>">
                                            <i class="fa fa-edit"></i>
                                            <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="#" class="tip btn btn-danger bpo" title="<b><?= lang("delete_product") ?></b>"
                                            data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= admin_url('products/delete/' . $product->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                                            data-html="true" data-placement="top">
                                            <i class="fa fa-trash-o"></i>
                                            <span class="hidden-sm hidden-xs"><?= lang('delete') ?></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                            $(document).ready(function () {
                                $('.tip').tooltip();
                            });
                            </script>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('.change_img').click(function(event) {
        event.preventDefault();
        var img_src = $(this).attr('href');
        $('#pr-image').attr('src', img_src);
        return false;
    });
});
</script>
