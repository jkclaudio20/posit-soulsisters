<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .table th {
        text-align: center;
    }

    .table td {
        padding: 2px;
    }

    .table td .table td:nth-child(odd) {
        text-align: left;
    }

    .table td .table td:nth-child(even) {
        text-align: right;
    }

    .table a:hover {
        text-decoration: none;
    }

    .cl_wday {
        text-align: center;
        font-weight: bold;
    }

    .cl_equal {
        width: 14%;
    }

    td.day {
        width: 14%;
        padding: 0 !important;
        vertical-align: top !important;
    }

    .day_num {
        width: 100%;
        text-align: left;
        margin: 0;
        padding: 8px;
    }

    .day_num:hover {
        background: #F5F5F5;
    }

    .content {
        width: 100%;
        text-align: left;
        color: #428bca;
        padding: 8px;
    }

    .highlight {
        color: #0088CC;
        font-weight: bold;
    }
</style>

<div class="padding">
    <div class="row m-b">
        <div class="col-xs-12 col-lg-12">
            <div class="box animated fadeIn">
                <div class="box-header dker">
                    <h3"><i class="fa-fw fa fa-calendar"></i><?= lang('daily_purchases').' ('.($sel_warehouse ? $sel_warehouse->name : lang('all_warehouses')).')'; ?></h3>

                    <div class="box-tool">
                        <ul class="nav">
                            <?php if (!empty($warehouses) && !$this->session->userdata('warehouse_id')) { ?>
                                <li class="nav-item inline dropdown">
                                    <a data-toggle="dropdown" class="nav-link" href="#"><i class="material-icons tip" data-placement="left" title="<?=lang("warehouses")?>">home</i></a>
                                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                        <li><a href="<?=admin_url('reports/daily_purchases/0/'.$year.'/'.$month)?>"><i class="fa fa-building-o"></i> <?=lang('all_warehouses')?></a></li>
                                        <li class="divider"></li>
                                        <?php
                                            foreach ($warehouses as $warehouse) {
                                                    echo '<li><a href="' . admin_url('reports/daily_purchases/'.$warehouse->id.'/'.$year.'/'.$month) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                                }
                                            ?>
                                    </ul>
                                </li>
                            <?php } ?>
                            <li class="nav-item inline dropdown">
                                <a href="#" id="image" class="nav-link tip" title="<?= lang('save_image') ?>">
                                    <i class="material-icons">image</i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <small><?= lang('get_day_profit').' '.lang("reports_calendar_text") ?></small>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div>
                                <?php echo $calender; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.table .day_num').click(function () {
            var day = $(this).html();
            var date = '<?= $year.'-'.$month.'-'; ?>'+day;
            var href = '<?= admin_url('reports/profit'); ?>/'+date+'/<?= ($warehouse_id ? $warehouse_id : ''); ?>';
            $.get(href, function( data ) {
                $("#myModal").html(data).modal();
            });

        });
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/daily_purchases/'.($warehouse_id ? $warehouse_id : 0).'/'.$year.'/'.$month.'/pdf')?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
    });
</script>
