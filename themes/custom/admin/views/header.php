<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <base href="<?= site_url() ?>"/>
  <title><?= $page_title ?> - <?= $Settings->site_name ?></title>
  <meta name="description" content="Admin, Dashboard, Bootstrap, Bootstrap 4, Angular, AngularJS" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="<?= $assets ?>images/posit-favicon.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" href="<?= $assets ?>images/posit-favicon.png"/>

  <!-- style -->
  <!-- <link href="<?= $assets ?>styles/theme.css" rel="stylesheet"/> -->
  <!-- <link href="<?= $assets ?>styles/style.css" rel="stylesheet"/> -->
  <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
  <script type="text/javascript" src="<?= $assets ?>js/jquery-migrate-1.2.1.min.js"></script>
  <script src="<?= $assets ?>libs/jquery/jquery/dist/jquery.js"></script>
  <script type="text/javascript" src="<?= $assets; ?>js/hc/highcharts.js"></script>
  <!-- Bootstrap -->
    <script src="<?= $assets ?>libs/jquery/tether/dist/js/tether.min.js"></script>
    <script src="<?= $assets ?>libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
    <script src='<?= $assets ?>fullcalendar/js/moment.min.js'></script>
  <script src="<?= $assets ?>fullcalendar/js/fullcalendar.min.js"></script>
  <script src="<?= $assets ?>fullcalendar/js/lang-all.js"></script>
  <script src='<?= $assets ?>fullcalendar/js/bootstrap-colorpicker.min.js'></script>
  <script src='<?= $assets ?>fullcalendar/js/main.js'></script>
  <script src="<?= $assets ?>bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <link rel="stylesheet" href="<?= $assets ?>animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="<?= $assets ?>glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="<?= $assets ?>font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="<?= $assets ?>material-design-icons/material-design-icons.css" type="text/css" />

  <link rel="stylesheet" type="text/css" href="<?= $assets ?>libs/jquery/iCheck/icheck.css" />
  <link rel="stylesheet" type="text/css" href="<?= $assets ?>libs/jquery/iCheck/skins/all.css" />
  <script type="text/javascript" src="<?= $assets ?>libs/jquery/iCheck/icheck.min.js"></script>

  <link href='<?= $assets ?>fullcalendar/css/fullcalendar.min.css' rel='stylesheet' />
<link href='<?= $assets ?>fullcalendar/css/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href="<?= $assets ?>fullcalendar/css/bootstrap-colorpicker.min.css" rel="stylesheet" />

<!-- PNotify -->
<link href="<?= $assets ?>pnotify/pnotify.custom.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= $assets ?>pnotify/pnotify.custom.min.js"></script>
<!-- /PNotify -->

<link href="<?= $assets ?>bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet" />
<link href="<?= $assets ?>parsleyjs/src/parsley.css" rel="stylesheet" />


  <!-- <link rel="stylesheet" href="<?= $assets ?>js/malihu-custom-scrollbar/jquery.mCustomScrollbar.min.css" type="text/css" /> -->
  <!-- Datatables -->
    <!-- <script src="<?= $assets ?>js/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?= $assets ?>js/plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <link rel="stylesheet" href="<?= $assets ?>js/datatables/media/extensions/datatables/css/dataTables.bootstrap4.css" type="text/css" />
    <link rel="stylesheet" href="<?= $assets ?>js/datatables/media/extensions/buttons/css/buttons.bootstrap4.min.css" type="text/css" />
    <script src="<?= $assets ?>js/datatables/media/extensions/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= $assets ?>js/datatables/media/extensions/buttons/js/buttons.html5.min.js"></script>
    <script src="<?= $assets ?>js/datatables/media/extensions/buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="<?= $assets ?>js/datatables/media/extensions/buttons/js/buttons.print.min.js"></script>
    <script src="<?= $assets ?>js/datatables/media/extensions/pdfmake/pdfmake.min.js"></script>
    <script src="<?= $assets ?>js/datatables/media/extensions/pdfmake/vfs_fonts.js"></script>
    <script src="<?= $assets ?>js/datatables/media/extensions/jszip/jszip.min.js"></script> -->
    <!-- /Datatables
-->
  <link rel="stylesheet" href="<?= $assets ?>bootstrap/dist/css/bootstrap.min.css" type="text/css" />

  <!-- <link rel="stylesheet" href="<?= $assets ?>libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.4.css" type="text/css" />
  <link rel="stylesheet" href="<?= $assets ?>libs/jquery/select2/dist/css/select2.min.css" type="text/css" /> -->
  <!-- build:css <?= $assets ?>styles/app.min.css -->
  <link rel="stylesheet" href="<?= $assets ?>styles/app.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="<?= $assets ?>styles/font.css" type="text/css" />
  <script type="text/javascript">
      $(window).load(function () {
          $("#loading").fadeOut("slow");
      });
  </script>
</head>
<body>
  <div id="loading"></div>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  <div id="aside" class="app-aside modal fade md show-text nav-dropdown folded">
    <div class="left navside white dk" layout="column">
      <div class="navbar no-radius success">
        <!-- brand -->
        <a class="navbar-brand" href="<?= admin_url(); ?>">
            <span class="hidden-folded inline"><?= $Settings->site_name ?></span>
        </a>
        <!-- / brand -->
      </div>
      <div flex class="hide-scroll">
          <nav class="scroll nav-active-success">

              <ul class="nav" ui-nav>
                <li class="nav-header hidden-folded">
                  <small class="text-muted">Main</small>
                </li>

                <li>
                  <a href="<?= admin_url('welcome'); ?>">
                    <span class="nav-icon">
                      <i class="fa fa-dashboard"></i>
                    </span>
                    <span class="nav-text">Dashboard</span>
                  </a>
                </li>

                <?php if ($Owner || $Admin): ?>
                    <li>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                               <i class="fa fa-barcode"></i>
                            </span>
                            <span class="nav-text">Products</span>
                        </a>
                        <ul class="nav-sub nav-mega">
                            <li>
                                <a href="<?= admin_url('products'); ?>">
                                    <span class="nav-text">Products List</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('products/add'); ?>">
                                    <span class="nav-text">Add Product</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('products/import_csv'); ?>">
                                    <span class="nav-text">Import Products</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('products/print_barcodes'); ?>">
                                    <span class="nav-text">Print Barcodes</span>
                                </a>
                            </li>
                          
                            <li>
                                <a href="<?= admin_url('products/quantity_adjustments'); ?>">
                                    <span class="nav-text">Quantity Adjustments</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('products/add_adjustment'); ?>">
                                    <span class="nav-text">Add Adjustment</span>
                                </a>
                            </li>
                          
                            <li>
                                <a href="<?= admin_url('products/stock_counts'); ?>">
                                    <span class="nav-text">Stock Counts</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('products/count_stock'); ?>">
                                    <span class="nav-text">Count Stock</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <!-- Sales -->
                    <li>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                               <i class="fa fa-heart"></i>
                            </span>
                            <span class="nav-text">Sales</span>
                        </a>
                        <ul class="nav-sub nav-mega">
                            <li>
                                <a href="<?= admin_url('sales'); ?>">
                                    <span class="nav-text">Sales List</span>
                                </a>
                            </li>

                            <?php if (POS): ?>
                                <li>
                                    <a href="<?= admin_url('pos/sales'); ?>">
                                        <span class="nav-text">POS Sales</span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li>
                                <a href="<?= admin_url('sales/add'); ?>">
                                    <span class="nav-text">Add Sale</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('sales/sale_by_csv'); ?>">
                                    <span class="nav-text">Add Sale by CSV</span>
                                </a>
                            </li>
                          
                            <li>
                                <a href="<?= admin_url('sales/deliveries'); ?>">
                                    <span class="nav-text">Deliveries</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('sales/gift_cards'); ?>">
                                    <span class="nav-text">Gift Cards</span>
                                </a>
                            </li>
                          
                        </ul>
                    </li>
                 
                    <li>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                               <i class="fa fa-heart-o"></i>
                            </span>
                            <span class="nav-text">Quotations</span>
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="<?= admin_url('quotes'); ?>">
                                    <span class="nav-text">Quotations List</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('quotes/add'); ?>">
                                    <span class="nav-text">Add Quotation</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                  
                    <li>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                               <i class="fa fa-star"></i>
                            </span>
                            <span class="nav-text">Purchases</span>
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="<?= admin_url('purchases'); ?>">
                                    <span class="nav-text">Purchases List</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('purchases/add'); ?>">
                                    <span class="nav-text">Add Purchase</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('purchases/expenses'); ?>">
                                    <span class="nav-text">Expenses</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('purchases/add_expense'); ?>" data-toggle="modal" data-target="#myModal">
                                    <span class="nav-text">Add Expense</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                  
                    <li>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                               <i class="fa fa-star-o"></i>
                            </span>
                            <span class="nav-text">Transfers</span>
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="<?= admin_url('transfers'); ?>">
                                    <span class="nav-text">Transfers List</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('transfers/add'); ?>">
                                    <span class="nav-text">Add Transfer</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('transfers/transfer_by_csv'); ?>">
                                    <span class="nav-text">Add Transfer by CSV</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                 
                    <li>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                               <i class="fa fa-random"></i>
                            </span>
                            <span class="nav-text">Returns</span>
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="<?= admin_url('returns'); ?>">
                                    <span class="nav-text">Returns List</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('returns/add'); ?>">
                                    <span class="nav-text">Add Return</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                               <i class="fa fa-users"></i>
                            </span>
                            <span class="nav-text">People</span>
                        </a>
                        <ul class="nav-sub nav-mega">
                            <?php if ($Owner): ?>
                               <!--  <li>
                                    <a href="<?= admin_url('members'); ?>">
                                        <span class="nav-text">Members List</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= admin_url('members/add'); ?>">
                                        <span class="nav-text">Add Member</span>
                                    </a>
                                </li>-->
                                <li> 
                                    <a href="<?= admin_url('users'); ?>">
                                        <span class="nav-text">Users List</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('users/create_user'); ?>">
                                        <span class="nav-text">Add User</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('billers'); ?>">
                                        <span class="nav-text">Billers List</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                        <span class="nav-text">Add Biller</span>
                                    </a>
                                </li>
                            <?php endif; ?>

                            <li>
                                <a href="<?= admin_url('customers'); ?>">
                                    <span class="nav-text">Customers List</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('customers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                    <span class="nav-text">Add Customer</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('suppliers'); ?>">
                                    <span class="nav-text">Suppliers List</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= admin_url('suppliers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                    <span class="nav-text">Add Supplier</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="<?= admin_url('notifications'); ?>">
                            <span class="nav-icon">
                                <i class="fa fa-info-circle"></i>
                            </span>
                            <span class="nav-text">Notifications</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= admin_url('calendar'); ?>">
                            <span class="nav-icon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <span class="nav-text">Calendar</span>
                        </a>
                    </li>

                    <?php if ($Owner): ?>
                        <li>
                            <a>
                                <span class="nav-caret">
                                    <i class="fa fa-caret-down"></i>
                                </span>
                                <span class="nav-icon">
                                   <i class="fa fa-cog"></i>
                                </span>
                                <span class="nav-text">Settings</span>
                            </a>
                            <ul class="nav-sub nav-mega">
                                <li>
                                    <a href="<?= admin_url('system_settings'); ?>">
                                        <span class="nav-text">System Settings</span>
                                    </a>
                                </li>

                                <!-- <li>
                                  <a href="<?= admin_url('system_settings/membership'); ?>">
                                    <span class="nav-text">Membership Settings</span>
                                  </a>
                                </li> -->

                                <?php if (POS): ?>
                                    <li>
                                        <a href="<?= admin_url('pos/settings'); ?>">
                                            <span class="nav-text">POS Settings</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?= admin_url('pos/printers'); ?>">
                                            <span class="nav-text">Printers</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?= admin_url('pos/add_printer'); ?>">
                                            <span class="nav-text">Add Printer</span>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <li>
                                    <a href="<?= admin_url('system_settings/change_logo'); ?>" data-toggle="modal" data-target="#myModal">
                                        <span class="nav-text">Change Logo</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/currencies'); ?>">
                                        <span class="nav-text">Currencies</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/customer_groups'); ?>">
                                        <span class="nav-text">Customer Groups</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/price_groups'); ?>">
                                        <span class="nav-text">Price Groups</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/categories'); ?>">
                                        <span class="nav-text">Categories</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/expense_categories'); ?>">
                                        <span class="nav-text">Expense Categories</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/units'); ?>">
                                        <span class="nav-text">Units</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/brands'); ?>">
                                        <span class="nav-text">Brands</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/variants'); ?>">
                                        <span class="nav-text">Variants</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/tax_rates'); ?>">
                                        <span class="nav-text">Tax Rates</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/warehouses'); ?>">
                                        <span class="nav-text">Warehouses</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/email_templates'); ?>">
                                        <span class="nav-text">Email Templates</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/user_groups'); ?>">
                                        <span class="nav-text">Group Permissions</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('system_settings/backups'); ?>">
                                        <span class="nav-text">Backups</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a>
                                <span class="nav-caret">
                                    <i class="fa fa-caret-down"></i>
                                </span>
                                <span class="nav-icon">
                                   <i class="fa fa-bar-chart-o"></i>
                                </span>
                                <span class="nav-text">Reports</span>
                            </a>
                            <ul class="nav-sub">
                                <li>
                                    <a href="<?= admin_url('reports'); ?>">
                                        <span class="nav-text">Overview Chart</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/warehouse_stock'); ?>">
                                        <span class="nav-text">Warehouse Stock</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/best_sellers'); ?>">
                                        <span class="nav-text">Best Sellers</span>
                                    </a>
                                </li>

                                <?php if (POS): ?>
                                    <li>
                                        <a href="<?= admin_url('reports/register'); ?>">
                                            <span class="nav-text">Register Report</span>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <li>
                                    <a href="<?= admin_url('reports/quantity_alerts'); ?>">
                                        <span class="nav-text">Product Quantity Alerts</span>
                                    </a>
                                </li>

                                <?php if ($Settings->product_expiry): ?>
                                    <li>
                                        <a href="<?= admin_url('reports/expiry_alerts'); ?>">
                                            <span class="nav-text">Product Expiry Alerts</span>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <li>
                                    <a href="<?= admin_url('reports/products'); ?>">
                                        <span class="nav-text">Products Report</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/adjustments'); ?>">
                                        <span class="nav-text">Adjustments Report</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/categories'); ?>">
                                        <span class="nav-text">Categories Report</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/brands'); ?>">
                                        <span class="nav-text">Brands Report</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/daily_sales'); ?>">
                                        <span class="nav-text">Daily Sales</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/monthly_sales'); ?>">
                                        <span class="nav-text">Monthly Sales</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/sales'); ?>">
                                        <span class="nav-text">Sales Report</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/payments'); ?>">
                                        <span class="nav-text">Payments Report</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/tax'); ?>">
                                        <span class="nav-text">Tax Report</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/profit_loss'); ?>">
                                        <span class="nav-text">Profit and Loss</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/daily_purchases'); ?>">
                                        <span class="nav-text">Daily Purchases</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/monthly_purchases'); ?>">
                                        <span class="nav-text">Monthly Purchases</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/expenses'); ?>">
                                        <span class="nav-text">Expenses Report</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/customers'); ?>">
                                        <span class="nav-text">Customers Report</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/suppliers'); ?>">
                                        <span class="nav-text">Suppliers Report</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= admin_url('reports/users'); ?>">
                                        <span class="nav-text">Staff Report</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <?php if ($Owner && file_exists(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'Shop.php')): ?>
                            <li>
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                       <i class="fa fa-shopping-cart"></i>
                                    </span>
                                    <span class="nav-text"><?= lang('front_end'); ?></span>
                                </a>
                                <ul class="nav-sub">
                                    <li>
                                        <a href="<?= admin_url('shop_settings'); ?>">
                                            <span class="nav-text">Shop Settings</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?= admin_url('shop_settings/slider'); ?>">
                                            <span class="nav-text">Slider Settings</span>
                                        </a>
                                    </li>

                                    <?php if ($Settings->apis): ?>
                                        <li>
                                            <a href="<?= admin_url('api_settings'); ?>">
                                                <span class="nav-text">API Keys</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <li>
                                        <a href="<?= admin_url('shop_settings/pages'); ?>">
                                            <span class="nav-text">Pages List</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?= admin_url('shop_settings/add_page'); ?>">
                                            <span class="nav-text">Add Page</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?= admin_url('shop_settings/sms_settings'); ?>">
                                            <span class="nav-text">SMS Settings</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?= admin_url('shop_settings/send_sms'); ?>">
                                            <span class="nav-text">Send SMS</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="<?= admin_url('shop_settings/sms_log'); ?>">
                                            <span class="nav-text">SMS Log</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if ($GP['products-index'] || $GP['products-add'] || $GP['products-barcode'] || $GP['products-adjustments'] || $GP['products-stock_count']): ?>
                            <li>
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                       <i class="fa fa-barcodes"></i>
                                    </span>
                                    <span class="nav-text">Products</span>
                                </a>
                                <ul class="nav-sub">
                                    <li>
                                        <a href="<?= admin_url('products'); ?>">
                                            <span class="nav-text">Products List</span>
                                        </a>
                                    </li>

                                    <?php if ($GP['products-add']): ?>
                                        <li>
                                            <a href="<?= admin_url('products/add'); ?>">
                                                <span class="nav-text">Add Product</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($GP['print-barcodes']): ?>
                                        <li>
                                            <a href="<?= admin_url('products/print_barcodes'); ?>">
                                                <span class="nav-text">Print Barcodes</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($GP['products-adjustments']): ?>
                                        <li>
                                            <a href="<?= admin_url('products/quantity_adjustments'); ?>">
                                                <span class="nav-text">Quantity Adjustments</span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?= admin_url('products/add_adjustment'); ?>">
                                                <span class="nav-text">Add Adjustment</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($GP['products-stock-count']): ?>
                                        <li>
                                            <a href="<?= admin_url('products/stock_counts'); ?>">
                                                <span class="nav-text">Stock Counts</span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="<?= admin_url('products/count_stock'); ?>">
                                                <span class="nav-text">Count Stock</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        <?php endif; ?>

                        <?php if ($GP['sales-index'] || $GP['sales-add'] || $GP['sales-deliveries'] || $GP['sales-gift_cards']): ?>
                             <li>
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                       <i class="fa fa-heart"></i>
                                    </span>
                                    <span class="nav-text">Sales</span>
                                </a>
                                <ul class="nav-sub">
                                    <li>
                                        <a href="<?= admin_url('sales'); ?>">
                                            <span class="nav-text">Sales List</span>
                                        </a>
                                    </li>

                                    <?php if (POS && $GP['pos-index']): ?>
                                        <li>
                                            <a href="<?= admin_url('pos/sales'); ?>">
                                                <span class="nav-text">POS Sales</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($GP['sales-add']): ?>
                                        <li>
                                            <a href="<?= admin_url('sales/add'); ?>">
                                                <span class="nav-text">Add Sale</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($GP['sales-deliveries']): ?>
                                        <li>
                                            <a href="<?= admin_url('sales/deliveries'); ?>">
                                                <span class="nav-text">Deliveries</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($GP['sales-gift_cards']): ?>
                                        <li>
                                            <a href="<?= admin_url('sales/gift_cards'); ?>">
                                                <span class="nav-text">Gift Cards</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        <?php endif; ?>

                        <?php if ($GP['quotes-index'] || $GP['quotes-add']): ?>
                            <li>
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                       <i class="fa fa-heart-o"></i>
                                    </span>
                                    <span class="nav-text">Quotations</span>
                                </a>
                                <ul class="nav-sub">
                                    <li>
                                        <a href="<?= admin_url('quotes'); ?>">
                                            <span class="nav-text">Quotations</span>
                                        </a>
                                    </li>

                                    <?php if ($GP['quotes-add']): ?>
                                        <li>
                                            <a href="<?= admin_url('quotes/add'); ?>">
                                                <span class="nav-text">Add Quotation</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        <?php endif; ?>

                         <?php if ($GP['purchases-index'] || $GP['purchases-add'] || $GP['purchases-expenses']): ?>
                            <li>
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                       <i class="fa fa-star"></i>
                                    </span>
                                    <span class="nav-text">Purchases</span>
                                </a>
                                <ul class="nav-sub">
                                    <li>
                                        <a href="<?= admin_url('purchases'); ?>">
                                            <span class="nav-text">Purchases List</span>
                                        </a>
                                    </li>

                                    <?php if ($GP['purchases-add']): ?>
                                        <li>
                                            <a href="<?= admin_url('purchases/add'); ?>">
                                                <span class="nav-text">Add Purchase</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($GP['purchases-expenses']): ?>
                                        <li>
                                            <a href="<?= admin_url('purchases/expenses'); ?>">
                                                <span class="nav-text">Expenses</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?= admin_url('purchases/add_expense'); ?>">
                                                <span class="nav-text">Add Expense</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                         <?php endif; ?>

                         <?php if ($GP['transfers-index'] || $GP['transfers-add']): ?>
                            <li>
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                       <i class="fa fa-star-o"></i>
                                    </span>
                                    <span class="nav-text">Transfers</span>
                                </a>
                                <ul class="nav-sub">
                                    <li>
                                        <a href="<?= admin_url('transfers'); ?>">
                                            <span class="nav-text">Transfers List</span>
                                        </a>
                                    </li>

                                    <?php if ($GP['transfers-add']): ?>
                                        <li>
                                            <a href="<?= admin_url('transfers/add'); ?>">
                                                <span class="nav-text">Add Transfer</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                         <?php endif; ?>

                         <?php if ($GP['returns-index'] || $GP['returns-add']): ?>
                            <li>
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                       <i class="fa fa-random"></i>
                                    </span>
                                    <span class="nav-text">Returns</span>
                                </a>
                                <ul class="nav-sub">
                                    <li>
                                        <a href="<?= admin_url('returns'); ?>">
                                            <span class="nav-text">Returns List</span>
                                        </a>
                                    </li>

                                    <?php if ($GP['returns-add']): ?>
                                        <li>
                                            <a href="<?= admin_url('returns/add'); ?>">
                                                <span class="nav-text">Add Return</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                         <?php endif; ?>

                         <?php if ($GP['customers-index'] || $GP['customers-add'] || $GP['suppliers-index'] || $GP['suppliers-add']): ?>
                            <li>
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                       <i class="fa fa-users"></i>
                                    </span>
                                    <span class="nav-text">People</span>
                                </a>
                                <ul class="nav-sub">
                                    <?php if ($GP['customers-index']): ?>
                                        <li>
                                            <a href="<?= admin_url('customers'); ?>">
                                                <span class="nav-text">Customers List</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($GP['customers-add']): ?>
                                        <li>
                                            <a href="<?= admin_url('customers/add'); ?>">
                                                <span class="nav-text">Add Customer</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($GP['suppliers-index']): ?>
                                        <li>
                                            <a href="<?= admin_url('suppliers'); ?>">
                                                <span class="nav-text">Suppliers List</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($GP['suppliers-add']): ?>
                                        <li>
                                            <a href="<?= admin_url('suppliers/add'); ?>">
                                                <span class="nav-text">Add Supplier</span>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </li>
                         <?php endif; ?>

                    <?php endif; ?>
                <?php endif; ?>
              </ul>
          </nav>
      </div>
      <div flex-no-shrink>
        <div class="nav-fold">
        <a href="#/app/page/profile" ui-sref="app.page.profile" >
        <span class="pull-right m-v-sm hidden-folded">
        <b class="label warn">9</b>
        </span>
        <span class="pull-left">
        <img alt="" class="w-40 img-circle" src="<?= $this->session->userdata('avatar') ? base_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : base_url('assets/images/' . $this->session->userdata('gender') . '.png'); ?>" class="mini_avatar img-rounded">
        </span>
        <span class="clear hidden-folded p-x">
          <span class="block _500"><?= $this->session->userdata('username'); ?></span>
        </span>
        </a>
        </div>
      </div>
    </div>
  </div>
  <!-- / aside -->

  <div id="content" class="app-content box-shadow-z3" role="main">
    <div class="app-header success box-shadow">
        <div class="navbar navbar-toggleable-sm flex-row align-items-center">
            <!-- Open side - Naviation on mobile -->
            <a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
              <i class="material-icons">&#xe5d2;</i>
            </a>
            <!-- / -->

            <!-- Page title - Bind to $state's title -->
            <div class="mb-0 h5 no-wrap" ng-bind="$state.current.data.title" id="pageTitle"><?= $page_title; ?></div>

            <!-- navbar collapse -->
            <!-- / navbar collapse -->

            <!-- navbar right -->
            <ul class="nav navbar-nav ml-auto flex-row">
              <?php if (SHOP) { ?>
                <li class="nav-item dropdown pos-stc-xs">
                  <a class="nav-link mr-2" href="<?= site_url('/'); ?>" class="btn">
                    <span class="fa fa-shopping-cart"></span>
                  </a>
                </li>
                <?php } ?>

                <?php if ($Owner) { ?>
                  <li class="dropdown pos-stc-xs">
                      <a class="nav-link mr-2" title="<?= lang('settings') ?>" data-placement="bottom" href="<?= admin_url('system_settings') ?>">
                          <i class="fa fa-cogs"></i>
                      </a>
                  </li>
                <?php } ?>

                <li class="dropdown hidden-xs">
                    <a class="nav-link mr-2" title="<?= lang('calculator') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                        <i class="fa fa-calculator"></i>
                    </a>
                    <ul class="dropdown-menu pull-right calc">
                        <li class="dropdown-content">
                            <span id="inlineCalc"></span>
                        </li>
                    </ul>
                </li>

              <?php if (($Owner || $Admin || $GP['reports-quantity_alerts'] || $GP['reports-expiry_alerts']) && ($qty_alert_num > 0 || $exp_alert_num > 0 || $shop_sale_alerts)) { ?>
                  <li class="dropdown post-stc-xs">
                      <a class="nav-link mr-2" title="<?= lang('alerts') ?>" data-placement="left" data-toggle="dropdown" href="#">
                          <i class="fa fa-exclamation-triangle"></i>
                          <span class="label label-sm up danger"><?= $qty_alert_num+(($Settings->product_expiry) ? $exp_alert_num : 0)+$shop_sale_alerts+$shop_payment_alerts; ?></span>
                      </a>
                      <ul class="dropdown-menu pull-right content-scroll">
                          <?php if ($qty_alert_num > 0) { ?>
                          <li>
                              <a href="<?= admin_url('reports/quantity_alerts') ?>" class="">
                                  <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_num; ?></span>
                                  <span style="padding-right: 35px;"><?= lang('quantity_alerts') ?></span>
                              </a>
                          </li>
                          <?php } ?>
                          <?php if ($Settings->product_expiry) { ?>
                          <li>
                              <a href="<?= admin_url('reports/expiry_alerts') ?>" class="">
                                  <span class="label label-danger pull-right" style="margin-top:3px;"><?= $exp_alert_num; ?></span>
                                  <span style="padding-right: 35px;"><?= lang('expiry_alerts') ?></span>
                              </a>
                          </li>
                          <?php } ?>
                          <?php if ($shop_sale_alerts) { ?>
                          <li>
                              <a href="<?= admin_url('sales?shop=yes&delivery=no') ?>" class="">
                                  <span class="label label-danger pull-right" style="margin-top:3px;"><?= $shop_sale_alerts; ?></span>
                                  <span style="padding-right: 35px;"><?= lang('sales_x_delivered') ?></span>
                              </a>
                          </li>
                          <?php } ?>
                          <?php if ($shop_payment_alerts) { ?>
                          <li>
                              <a href="<?= admin_url('sales?shop=yes&attachment=yes') ?>" class="">
                                  <span class="label label-danger pull-right" style="margin-top:3px;"><?= $shop_payment_alerts; ?></span>
                                  <span style="padding-right: 35px;"><?= lang('manual_payments') ?></span>
                              </a>
                          </li>
                          <?php } ?>
                      </ul>
                  </li>
                <?php } ?>

                 <?php if (!$info) { ?>
                    <li class="dropdown post-stc-xs">
                        <a class="nav-link mr-2" title="<?= lang('notifications') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                            <i class="fa fa-info-circle"></i>
                            <span class="label label-sm up warn"><?= sizeof($info) ?></span>
                        </a>
                        <ul class="dropdown-menu pull-right content-scroll">
                            <li class="dropdown-header"><i class="fa fa-info-circle"></i> <?= lang('notifications'); ?></li>
                            <li class="dropdown-content">
                                <div class="scroll-div">
                                    <div class="top-menu-scroll">
                                        <ol class="oe">
                                            <?php foreach ($info as $n) {
                                                echo '<li>' . $n->comment . '</li>';
                                            } ?>
                                        </ol>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                <?php } ?>

                <li class="nav-item dropdown pos-stc-xs">
                   <a href="<?= admin_url('calendar') ?>" class="nav-link mr-2">
                      <span class="fa fa-calendar"></span>
                  </a>
                </li>

                <?php if (POS): ?>
                    <li class="dropdown nav-item hidden-xs">
                        <a class="btn tip nav-link mr-2" title="<?= lang('pos') ?>" data-placement="bottom" href="<?= admin_url('pos') ?>">
                            <i class="fa fa-th-large"></i> <span class="padding05"><?= lang('pos') ?></span>
                        </a>
                    </li>
                <?php endif; ?>

                <li class="nav-item dropdown pos-stc-xs">
                    <a href="<?= admin_url('users/profile/' . $this->session->userdata('user_id')); ?>" class="nav-link mr-2">
                      <span class="fa fa-user"></span>
                  </a>
                </li>

                <li class="nav-item dropdown pos-stc-xs">
                  <a href="<?= admin_url('logout'); ?>" class="nav-link mr-2">
                      <span class="fa fa-sign-out"></span>
                  </a>
                </li>

              <li class="nav-item hidden-md-up">
                <a class="nav-link pl-2" data-toggle="collapse" data-target="#collapse">
                  <i class="material-icons">&#xe5d4;</i>
                </a>
              </li>
            </ul>
            <!-- / navbar right -->
        </div>
    </div>
    <div class="app-footer">
      <div class="p-2 text-xs">
        <div class="pull-right text-muted py-1">
          Copyright &copy; 2018 <strong>TrendMedia Inc.</strong> <span class="hidden-xs-down">- POS with Membership Management System</span>
          <a ui-scroll-to="content"><i class="fa fa-long-arrow-up p-x-sm"></i></a>
        </div>
      </div>
    </div>
    <div ui-view class="app-body" id="view">
