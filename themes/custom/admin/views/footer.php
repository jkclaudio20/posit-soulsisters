</div>
  </div>

<!-- ############ LAYOUT END-->

  </div>
<!-- build:js scripts/app.html.js -->

  <?php
  $s2_lang_file = read_file('./assets/config_dumps/s2_lang.js');
  foreach (lang('select2_lang') as $s2_key => $s2_line) {
    $s2_data[$s2_key] = str_replace(array('{', '}'), array('"+', '+"'), $s2_line);
  }
  $s2_file_date = $this->parser->parse_string($s2_lang_file, $s2_data, true);
  ?>

</div>
<div class="modal fade" id="myModal" data-backdrop="true">
    <div class="modal-dialog "><h3 class="text-center text-white"><i class="fa fa-refresh fa-spin"></i> Loading...</h3></div>
</div>
<div class="modal fade" id="myModal2" data-backdrop="true"></div>
<div class="modal fade cal_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-2x">&times;</i>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="error"></div>
                <form>
                    <input type="hidden" value="" name="eid" id="eid">
                    <div class="form-group">
                        <?= lang('title', 'title'); ?>
                        <?= form_input('title', set_value('title'), 'class="form-control tip" id="title" required="required"'); ?>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang('start', 'start'); ?>
                                <?= form_input('start', set_value('start'), 'class="form-control datetime" id="start" required="required"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang('end', 'end'); ?>
                                <?= form_input('end', set_value('end'), 'class="form-control datetime" id="end"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang('event_color', 'color'); ?>
                                <div class="input-group">
                                    <span class="input-group-addon" id="event-color-addon" style="width:2em;"></span>
                                    <input id="color" name="color" type="text" class="form-control input-md" readonly="readonly" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <?= lang('description', 'description'); ?>
                        <textarea class="form-control skip" id="description" name="description"></textarea>
                    </div>

                </form>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>
<?php unset($Settings->setting_id, $Settings->smtp_user, $Settings->smtp_pass, $Settings->smtp_port, $Settings->update, $Settings->reg_ver, $Settings->allow_reg, $Settings->default_email, $Settings->mmode, $Settings->timezone, $Settings->restrict_calendar, $Settings->restrict_user, $Settings->auto_reg, $Settings->reg_notification, $Settings->protocol, $Settings->mailpath, $Settings->smtp_crypto, $Settings->corn, $Settings->customer_group, $Settings->envato_username, $Settings->purchase_code); ?>
<script type="text/javascript">
var dt_lang = <?=$dt_lang?>, dp_lang = <?=$dp_lang?>, site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url(), 'assets' => $assets, 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;
var lang = {paid: '<?=lang('paid');?>', pending: '<?=lang('pending');?>', completed: '<?=lang('completed');?>', ordered: '<?=lang('ordered');?>', received: '<?=lang('received');?>', partial: '<?=lang('partial');?>', sent: '<?=lang('sent');?>', r_u_sure: '<?=lang('r_u_sure');?>', due: '<?=lang('due');?>', returned: '<?=lang('returned');?>', transferring: '<?=lang('transferring');?>', active: '<?=lang('active');?>', inactive: '<?=lang('inactive');?>', unexpected_value: '<?=lang('unexpected_value');?>', select_above: '<?=lang('select_above');?>', download: '<?=lang('download');?>'};
</script>
<?php
$s2_lang_file = read_file('./assets/config_dumps/s2_lang.js');
foreach (lang('select2_lang') as $s2_key => $s2_line) {
    $s2_data[$s2_key] = str_replace(array('{', '}'), array('"+', '+"'), $s2_line);
}
$s2_file_date = $this->parser->parse_string($s2_lang_file, $s2_data, true);
?>


<!-- core -->
<!-- Parsley.js -->
<script type="text/javascript" src="<?= $assets ?>parsleyjs/dist/parsley.min.js"></script>
<!-- /Parsley.js -->
  <script type="text/javascript" src="<?= $assets ?>js/ajax-requests.js"></script>
  <script src="<?= $assets ?>libs/jquery/underscore/underscore-min.js"></script>
  <script src="<?= $assets ?>libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="<?= $assets ?>libs/jquery/PACE/pace.min.js"></script>

  <script src="<?= $assets ?>scripts/config.lazyload.js"></script>
  <script src="<?= $assets ?>libs/js/malihu-custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>

  <script src="<?= $assets ?>scripts/palette.js"></script>
  <script src="<?= $assets ?>scripts/ui-load.js"></script>
  <!-- <script src="<?= $assets ?>scripts/ui-jp.js"></script> -->
  <script src="<?= $assets ?>scripts/ui-include.js"></script>
  <script src="<?= $assets ?>scripts/ui-device.js"></script>
  <script src="<?= $assets ?>scripts/ui-form.js"></script>
  <script src="<?= $assets ?>scripts/ui-nav.js"></script>
  <!-- <script src="<?= $assets ?>scripts/ui-screenfull.js"></script> -->
  <script src="<?= $assets ?>scripts/ui-scroll-to.js"></script>
  <script src="<?= $assets ?>scripts/ui-toggle-class.js"></script>

  <script src="<?= $assets ?>scripts/app.js"></script>

<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.dtFilter.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/fnReloadAjax.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/select2.min.js"></script>
<!-- <script type="text/javascript" src="<?= $assets ?>libs/jquery/select2/dist/js/select2.full.min.js"></script> -->
<script type="text/javascript" src="<?= $assets ?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.calculator.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/core.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/perfect-scrollbar.min.js"></script>


<?= ($m == 'purchases' && ($v == 'add' || $v == 'edit' || $v == 'purchase_by_csv')) ? '<script type="text/javascript" src="' . $assets . 'js/purchases.js"></script>' : ''; ?>
<?= ($m == 'transfers' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/transfers.js"></script>' : ''; ?>
<?= ($m == 'sales' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/sales.js"></script>' : ''; ?>
<?= ($m == 'returns' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/returns.js"></script>' : ''; ?>
<?= ($m == 'quotes' && ($v == 'add' || $v == 'edit')) ? '<script type="text/javascript" src="' . $assets . 'js/quotes.js"></script>' : ''; ?>
<?= ($m == 'products' && ($v == 'add_adjustment' || $v == 'edit_adjustment')) ? '<script type="text/javascript" src="' . $assets . 'js/adjustments.js"></script>' : ''; ?>

<script type="text/javascript" charset="UTF-8">var oTable = '', r_u_sure = "<?=lang('r_u_sure')?>";
    <?=$s2_file_date?>
    $.extend(true, $.fn.dataTable.defaults, {"oLanguage":<?=$dt_lang?>});
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
    $(window).load(function () {
        $('.mm_<?=$m?>').addClass('active');
        $('.mm_<?=$m?>').find("ul").first().slideToggle();
        $('#<?=$m?>_<?=$v?>').addClass('active');
        $('.mm_<?=$m?> a .chevron').removeClass("closed").addClass("opened");
    });
</script>

<?php if (isset($message) && isset($result)): ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
          PNotify.removeAll();

            new PNotify({
                title : '<?php echo ucfirst($result); ?>',
                text  : '<?php echo preg_replace('~[\n]~', '<br>', str_replace("'", "\'", $message)); ?>',
                type  : '<?php echo $result; ?>'
            });
        });
    </script>
<?php endif; ?>

  <!-- ajax -->
<!--   <script src="<?= $assets ?>libs/jquery/jquery-pjax/jquery.pjax.js"></script>
  <script src="<?= $assets ?>scripts/ajax.js"></script> -->
<!-- endbuild -->
</body>
</html>
