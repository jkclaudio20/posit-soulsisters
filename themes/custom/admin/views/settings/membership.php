<div class="padding">
	<div class="margin">
		<h5 class="mb-0 _300"><i class="material-icons">&#xe7fb;</i> Membership Settings</h5>
		<small class="text-muted"><?= lang('update_info'); ?></small>
	</div>
	<div class="row m-b">
		<div class="col-xs-12 col-lg-12">
			<div class="box animated fadeIn">
				<div class="nav-active-border b-success">
					<ul class="nav nav-md dker">
						<li class="nav-item inline">
							<a class="nav-link active" href="#types" data-toggle="tab">
								<span class="text-md">Membership Types</span>
								<small class="block text-muted hidden-xs">Manage Membership Types</small>
							</a>
						</li>
						<li class="nav-item inline">
							<a class="nav-link"  href="#ui_kits" data-toggle="tab">
								<span class="text-md">UI Kits</span>
								<small class="block text-muted hidden-xs">This is a sample</small>
							</a>
						</li>
						<li class="nav-item inline">
							<a class="nav-link" href="#mail" data-toggle="tab">
								<span class="text-md">Mail</span>
								<small class="block text-muted hidden-xs">This is a sample</small>
							</a>
						</li>
					</ul>
				</div>

				<div class="box-tool">
					<ul class="nav">
						<li data-toggle="dropdown" class="nav-item inline dropdown">
							<a class="nav-link" href="#"><i class="material-icons tip" data-title="Options">more_vert</i></a>
						</li>
						<ul class="dropdown-menu pull-right" role="menu" aria-labelledby="dLabel">
							<li>
								<a data-toggle="modal" data-target="#add-membership-modal">
									<i class="material-icons">add</i> Add New
								</a>
							</li>
							<li>
								<a data-toggle="modal" data-target="#delete-all-membership-modal">
									<i class="material-icons">delete</i> Delete All
								</a>
							</li>
						</ul>
					</ul>
				</div>

				<div class="tab-content clear b-t">
					<div id="types" class="tab-pane active">
						<div class="box-body">
							<?php echo form_hidden('refresh_table', 0); ?>
							<table class="table table-hover table-striped" id="plans-table">
								<thead>
									<tr>
										<th>#</th>
										<th>Membership Name</th>
										<th>Description</th>
										<th>Actions</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div id="ui_kits" class="tab-pane">
						<div class="box-body">
							UI Kits
						</div>
					</div>
					<div id="mail" class="tab-pane">
						<div class="box-body">
							Mail
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- MODALS -->
<div id="add-membership-modal" class="modal fade ajax-modal" data-backdrop="true" data-url="<?= admin_url('membership/add'); ?>">
	<?php echo admin_form_open('membership/ajax_add', 'data-parsley-validate id="add-membership-form"'); ?>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="fa fa-plus"></i> New Membership Type</h5>
			</div>
			<div class="modal-body">
				<h3 class="text-center text-muted"><i class="fa fa-refresh fa-spin"></i></h3>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn primary p-x-md" disabled="disabled">Save</button>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>

<div id="edit-membership-modal" class="modal fade ajax-modal" data-backdrop="true" data-url="<?= admin_url('membership/edit'); ?>">
	<?php echo admin_form_open('membership/ajax_edit', 'data-parsley-validate id="edit-membership-form"'); ?>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="fa fa-edit"></i> Edit Membership Type</h5>
			</div>
			<div class="modal-body">
				<h3 class="text-center text-muted"><i class="fa fa-refresh fa-spin"></i></h3>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn primary p-x-md" disabled="disabled">Save</button>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>

<div id="view-membership-modal" class="modal fade ajax-modal" data-backdrop="true" data-url="<?= admin_url('membership/view'); ?>">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><i class="fa fa-user"></i> Membership Type Details</h5>
			</div>
			<div class="modal-body">
				<h3 class="text-center text-muted"><i class="fa fa-refresh fa-spin"></i></h3>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="delete-membership-modal" class="modal fade" data-backdrop="true">
	<div class="row-col h-v">
     	<div class="row-cell v-m">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title"><i class="fa fa-question-circle"></i> Delete Confirmation</h5>
					</div>
					<div class="modal-body text-center">
						<p>
							Are you sure to delete <span id="name" class="_600"></span> from Membership Types?
						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn danger p-x-md confirm-delete" data-url="<?= admin_url('membership/ajax_delete') ?>">Delete</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /MODALS -->

<script type="text/javascript">
	jQuery(document).ready(function(){
		var table = jQuery('#plans-table').DataTable({
			bProcessing : true,
			sAjaxSource : '<?= admin_url('membership/json_data'); ?>',
			aoColumns   : [
				{ mData : 'counter', sWidth : '5%', sClass : 'text-center' },
				{ mData : 'name' },
				{ mData : 'description' },
				{ mData : 'actions' }
			]
		});

		jQuery(document).on('change', '#types [name="refresh_table"]', function(){
			if (jQuery(this).val() == 1) table.fnReloadAjax();
			jQuery(this).val(0);
		});

		/* Add Functions */
		jQuery(document).on('ifChecked', '[name="limited"]', function(){
			var container = jQuery('#limit-options');
			if (jQuery(this).val() == 1)
			{
				container.find('input, select').prop({disabled:false,required:true});
				container.slideDown();
			}
			else
			{
				container.slideUp();
				container.find('input, select').prop({disabled:true,required:false});
			}

			//console.log(jQuery(this).val());
		});

		jQuery(document).on('input', '#no_of_classes', function(){
			var elem = jQuery(this).siblings('.input-group-addon');

			if (jQuery(this).val() == 1)
			{
				elem.text('class');
			}
			else
			{
				elem.text('classes');
			}
		});
		/*! Add Functions */
	});
</script>
