<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        oTable = $('#CURData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('system_settings/getVariants') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{"bSortable": false, "mRender": checkbox}, null, {"bSortable": false}]
        });
    });
</script>

<div class="padding">
    <div class="row m-b">
        <div class="col-xs-12 col-lg-12">
            <div class="box animated fadeIn">
                <div class="box-header dker">
                    <h3><i class="fa-fw fa fa-list"></i><?= $page_title ?></h3>

                    <div class="box-tool">
                        <ul class="nav">
                            <li class="nav-item inline dropdown">
                                <a href="<?php echo admin_url('system_settings/add_variant'); ?>" class="nav-link" data-toggle="modal" data-target="#myModal"><i class="icon fa fa-plus"></i></a>
                            </li>
                        </ul>
                    </div>

                    <small><?php echo $this->lang->line("list_results"); ?></small>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="CURData" class="table table-bordered table-hover table-striped reports-table">
                                    <thead>
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkth" type="checkbox" name="check"/>
                                        </th>
                                        <th><?php echo $this->lang->line("name"); ?></th>
                                        <th style="width:65px;"><?php echo $this->lang->line("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="3" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
