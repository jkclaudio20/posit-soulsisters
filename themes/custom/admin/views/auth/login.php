<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Login - <?= $Settings->site_name ?></title>
  <meta name="description" content="Admin, Dashboard, Bootstrap, Bootstrap 4, Angular, AngularJS" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="<?= $assets; ?>images/posit-favicon.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" href="<?= $assets ?>images/posit-favicon.png"/>
  
  <!-- style -->
  <link rel="stylesheet" href="<?= $assets; ?>animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="<?= $assets; ?>glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="<?= $assets; ?>font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="<?= $assets; ?>material-design-icons/material-design-icons.css" type="text/css" />

  <link rel="stylesheet" href="<?= $assets; ?>bootstrap/dist/css/bootstrap.min.css" type="text/css" />
  <!-- build:css <?= $assets; ?>styles/app.min.css -->
  <link rel="stylesheet" href="<?= $assets; ?>styles/app.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="<?= $assets; ?>styles/font.css" type="text/css" />
</head>
<body>
  <div class="app" id="app">

<!-- ############ LAYOUT START-->
  <div class="center-block w-xxl w-auto-xs p-y-md">
    <div class="navbar">
       <?php if ($Settings->logo2) {
            echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '" style="margin-bottom:10px;" />';
        } ?>
    </div>
    <div class="p-a-md box-color r box-shadow-z1 text-color m-a" id="login">
      <div class="m-b text-sm">
        Sign in with your Account
      </div>
      <?php echo admin_form_open("auth/login", 'class="login" data-toggle="validator"'); ?>
        <div class="md-form-group float-label">
          <input type="email" name="identity" class="md-input" ng-model="user.email" required>
          <label>Email</label>
        </div>
        <div class="md-form-group float-label">
          <input type="password" name="password" class="md-input" ng-model="user.password" required>
          <label>Password</label>
        </div>      
        <div class="m-b-md">        
          <label class="md-check">
            <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?><i class="primary"></i> Keep me signed in
          </label>
        </div>
        <button type="submit" class="btn primary btn-block p-x-md">Sign in</button>
      </form>
    </div>

    <div class="p-v-lg text-center">
      <div class="m-b"><a ui-sref="access.forgot-password" href="#forgot_password" class="text-primary _600 forgot_password_link">Forgot password?</a></div>

      <?php if ($Settings->allow_reg): ?>
          <div>Do not have an account? <a ui-sref="access.signup" href="#register" class="text-primary _600 register_link">Sign up</a></div>
        <?php endif; ?>
    </div>


    <div id="forgot_password" style="display: none;">
            <div class=" container">
                <div class="login-form-div">
                    <div class="login-content">
                        <?php
                        if ($error) {
                            ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <ul class="list-group"><?= $error; ?></ul>
                            </div>
                            <?php
                        }
                        if ($message) {
                            ?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <ul class="list-group"><?= $message; ?></ul>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="div-title col-sm-12">
                            <h3 class="text-primary"><?= lang('forgot_password') ?></h3>
                        </div>
                        <?php echo admin_form_open("auth/forgot_password", 'class="login" data-toggle="validator"'); ?>
                        <div class="col-sm-12">
                            <p>
                                <?= lang('type_email_to_reset'); ?>
                            </p>
                            <div class="textbox-wrap form-group">
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-envelope"></i></span>
                                    <input type="email" name="forgot_email" class="form-control "
                                    placeholder="<?= lang('email_address') ?>" required="required"/>
                                </div>
                            </div>
                            <div class="form-action">
                                <a class="btn btn-success pull-left login_link" href="#login">
                                    <i class="fa fa-chevron-left"></i> <?= lang('back') ?>
                                </a>
                                <button type="submit" class="btn btn-primary pull-right">
                                    <?= lang('submit') ?> &nbsp;&nbsp; <i class="fa fa-envelope"></i>
                                </button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if ($Settings->allow_reg) {
            ?>
            <div id="register">
                <div class="container">
                    <div class="registration-form-div reg-content">
                        <?php echo admin_form_open("auth/register", 'class="login" data-toggle="validator"'); ?>
                        <div class="div-title col-sm-12">
                            <h3 class="text-primary"><?= lang('register_account_heading') ?></h3>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= lang('first_name', 'first_name'); ?>
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-user"></i></span>
                                    <input type="text" name="first_name" class="form-control " placeholder="<?= lang('first_name') ?>" required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= lang('last_name', 'last_name'); ?>
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-user"></i></span>
                                    <input type="text" name="last_name" class="form-control " placeholder="<?= lang('last_name') ?>" required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= lang('company', 'company'); ?>
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-building"></i></span>
                                    <input type="text" name="company" class="form-control " placeholder="<?= lang('company') ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= lang('phone', 'phone'); ?>
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-phone-square"></i></span>
                                    <input type="text" name="phone" class="form-control " placeholder="<?= lang('phone') ?>" required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= lang('username', 'username'); ?>
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-user"></i></span>
                                    <input type="text" name="username" class="form-control " placeholder="<?= lang('username') ?>" required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= lang('email', 'email'); ?>
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-envelope"></i></span>
                                    <input type="email" name="email" class="form-control " placeholder="<?= lang('email_address') ?>" required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo lang('password', 'password1'); ?>
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-key"></i></span>
                                    <?php echo form_password('password', '', 'class="form-control tip" id="password1" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-regexp-message="'.lang('pasword_hint').'"'); ?>
                                </div>
                                <span class="help-block"><?= lang('pasword_hint') ?></span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo lang('confirm_password', 'confirm_password'); ?>
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-key"></i></span>
                                    <?php echo form_password('confirm_password', '', 'class="form-control" id="confirm_password" required="required" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <a href="#login" class="btn btn-success pull-left login_link">
                                <i class="fa fa-chevron-left"></i> <?= lang('back') ?>
                            </a>
                            <button type="submit" class="btn btn-primary pull-right">
                                <?= lang('register_now') ?> <i class="fa fa-user"></i>
                            </button>
                        </div>

                        <?php echo form_close(); ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
  </div>

<!-- ############ LAYOUT END-->

  </div>
<!-- build:js scripts/app.html.js -->
<!-- jQuery -->
  <script src="<?= $assets; ?>libs/jquery/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="<?= $assets; ?>libs/jquery/tether/dist/js/tether.min.js"></script>
  <script src="<?= $assets; ?>libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
<!-- core -->
  <script src="<?= $assets; ?>libs/jquery/underscore/underscore-min.js"></script>
  <script src="<?= $assets; ?>libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js"></script>
  <script src="<?= $assets; ?>libs/jquery/PACE/pace.min.js"></script>

  <script src="<?= $assets; ?>scripts/config.lazyload.js"></script>

  <script src="<?= $assets; ?>scripts/palette.js"></script>
  <script src="<?= $assets; ?>scripts/ui-load.js"></script>
  <script src="<?= $assets; ?>scripts/ui-jp.js"></script>
  <script src="<?= $assets; ?>scripts/ui-include.js"></script>
  <script src="<?= $assets; ?>scripts/ui-device.js"></script>
  <script src="<?= $assets; ?>scripts/ui-form.js"></script>
  <!-- <script src="<?= $assets; ?>scripts/ui-nav.js"></script> -->
  <script src="<?= $assets; ?>scripts/ui-screenfull.js"></script>
  <script src="<?= $assets; ?>scripts/ui-scroll-to.js"></script>
  <script src="<?= $assets; ?>scripts/ui-toggle-class.js"></script>

  <script src="<?= $assets; ?>scripts/app.js"></script>

  <!-- ajax -->
  <script src="<?= $assets; ?>libs/jquery/jquery-pjax/jquery.pjax.js"></script>
  <script src="<?= $assets; ?>scripts/ajax.js"></script>
  <script src="<?= $assets ?>js/jquery.cookie.js"></script>
  <script src="<?= $assets ?>js/login.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            localStorage.clear();
            var hash = window.location.hash;
            if (hash && hash != '') {
                $("#login").hide();
                $(hash).show();
            }
        });
    </script>
<!-- endbuild -->
</body>
</html>
